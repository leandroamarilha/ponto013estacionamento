![Parking](Logo.png)

# Parking #
> Tranquilidade e segurança para seu veículo

Aplicação **API REST** com **.NET Core 3.1** para gerenciar um estacionamento de carros e motos.

### Instalação ###

Você deve ter o .NET SDK instalado.

```shell
.NET Core 3.1 é recomendado
Postman
```
**Download .NET Core:** https://dotnet.microsoft.com/download/dotnet-core/3.1

**Download Postman:** https://www.postman.com/downloads


### Arquivo de importação para Postman ###
Há disponibilizado no repositório um arquivo **Json** contendo configurações para acesso aos "endpoints" básicos da API.

**Download**: https://bitbucket.org/leandroamarilha/ponto013estacionamento/src/master/Parking.postman_collection.json

### Dúvidas ###
leandroamarilha@hotmail.com