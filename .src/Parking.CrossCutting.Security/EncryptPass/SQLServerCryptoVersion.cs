﻿namespace Parking.CrossCutting.Security.EncryptPass
{
    public enum SQLServerCryptoVersion
    {
        ///<summary>
        /// TripleDES/SHA1
        /// </summary> 
        V1 = 0x01,
        /// <summary>
        /// AES256/SHA256
        /// </summary>
        V2 = 0x02
    }
}