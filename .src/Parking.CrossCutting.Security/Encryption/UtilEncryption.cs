﻿using Parking.CrossCutting.Security.EncryptPass;
using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Parking.CrossCutting.Security.Encryption
{
    public static class UtilEncryption
    {
        public static string EncryptMD5(this string text, string key)
        {
            StringBuilder result = new StringBuilder();

            using (MD5 md5 = MD5.Create())
            {
                foreach (byte item in md5.ComputeHash(Encoding.UTF8.GetBytes(text + key)))
                {
                    result.Append(item.ToString("x2"));
                }
            }

            return result.ToString().ToUpper();
        }

        public static string EncryptMD5(this string text)
        {
            return EncryptMD5(text, string.Empty);
        }

        public static string EncryptSHA256(this string text, string key)
        {
            StringBuilder result = new StringBuilder();

            using (SHA256Managed crypt = new SHA256Managed())
            {
                foreach (byte theByte in crypt.ComputeHash(Encoding.ASCII.GetBytes(text + key), 0, Encoding.ASCII.GetByteCount(text + key)))
                {
                    result.Append(theByte.ToString("x2"));
                }
            }

            return result.ToString().ToUpper();
        }

        public static string DecryptByPass(this byte[] Password)
        {
            var passphrase = "CM2015";
            var decryptedText = SQLServerCryptoMethod.DecryptByPassPhrase(@passphrase, Password);

            return decryptedText;
        }

        public static byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }

        public static string HashHmac(string secret, string pass)
        {
            Encoding encoding = Encoding.UTF8;
            using (HMACSHA512 hmac = new HMACSHA512(encoding.GetBytes(pass)))
            {
                var key = encoding.GetBytes(secret);
                var hash = hmac.ComputeHash(key);
                return BitConverter.ToString(hash).ToLower().Replace("-", string.Empty);
            }
        }

        public static string GetHash(string input)
        {
            HashAlgorithm hashAlgorithm = new SHA256CryptoServiceProvider();
            byte[] byteValue = System.Text.Encoding.UTF8.GetBytes(input);
            byte[] byteHash = hashAlgorithm.ComputeHash(byteValue);
            return Convert.ToBase64String(byteHash);
        }
    }
}
