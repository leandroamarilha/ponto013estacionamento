﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace Parking.CrossCutting.Security.Encryption
{
    public static class TokenEncryption
    {
        public static SigningCredentials GetSigningCredentials(this string securityKey)
        {
            SymmetricSecurityKey symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(securityKey));
            SigningCredentials signingCredentials = new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha256Signature);
            return signingCredentials;
        }

        public static SymmetricSecurityKey GetSymmetricSecurityKey(this string securityKey)
        {
            SymmetricSecurityKey symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(securityKey));
            return symmetricSecurityKey;
        }
    }
}
