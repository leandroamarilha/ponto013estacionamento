﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Parking.Application.Interfaces;
using Parking.Application.ViewsModels.Login;
using Parking.CrossCutting.MainController;
using Parking.Domain.Interfaces.Notifications;
using System.Threading;
using System.Threading.Tasks;

namespace Parking.API.Authenticate.Controllers
{
    [Route("api/[controller]")]
    public class AuthController : MainController
    {
        private readonly IUserAppService _userAppService;

        public AuthController(INotifier notifier,
                              IUserAppService userAppService) : base(notifier)
        {
            _userAppService = userAppService;
        }

        [HttpPost("Login")]
        public async Task<IActionResult> Login(UserLoginViewModel loginViewModel)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            var result = await _userAppService.Login(loginViewModel);

            return CustomResponse(result);
        }
    }
}