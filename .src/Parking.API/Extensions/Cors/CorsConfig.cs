﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace Parking.API.Extensions.Cors
{
    public static class CorsConfig
    {
        private static readonly string SpecificOrigins = "_specificOrigins";

        public static void AddCorsExtensions(this IServiceCollection services)
        {
            services.AddCors(options =>
             {
                 options.AddPolicy(name: SpecificOrigins,
                 builder =>
                 {
                     builder.AllowAnyOrigin()
                            .AllowAnyHeader()
                            .AllowAnyMethod()
                            .SetIsOriginAllowedToAllowWildcardSubdomains();
                 });
             });

        }
        public static void AddCorsConfig(this IApplicationBuilder app)
        {
            app.UseCors(SpecificOrigins);
        }
    }
}
