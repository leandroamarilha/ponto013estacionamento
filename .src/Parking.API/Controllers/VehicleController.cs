﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Parking.Application.Interfaces;
using Parking.Application.ViewsModels;
using Parking.CrossCutting.MainController;
using Parking.Domain.Interfaces.Notifications;
using System.Threading.Tasks;

namespace Parking.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class VehicleController : MainController
    {
        private readonly IVehicleAppService _vehicleAppService;

        public VehicleController(INotifier notifier, IVehicleAppService vehicleAppService) : base(notifier)
        {
            _vehicleAppService = vehicleAppService;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var vehicles = await _vehicleAppService.GetAll();

            if (vehicles == null) return NotFound();

            return Ok(vehicles);
        }

        // GET api/<VehicleController>/5
        [HttpGet("{id:int}")]
        public async Task<IActionResult> Get(int id)
        {
            if (id == 0) return BadRequest();

            var vehicle = await _vehicleAppService.GetbyId(id);

            if (vehicle == null) return NotFound();

            return Ok(vehicle);
        }

        [HttpGet("GetFullById/{id:int}")]
        public async Task<IActionResult> GetFullById(int id)
        {
            if (id == 0) return BadRequest();

            var vehicle = await _vehicleAppService.GetFullById(id);

            if (vehicle == null) return NotFound();

            return Ok(vehicle);
        }

        // POST api/<VehicleController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] VehicleViewModel vehicleViewModel)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            var vehicleViewModelResult = await _vehicleAppService.InsertWithReturn(vehicleViewModel);

            return CustomResponse(vehicleViewModelResult);
        }

        // PUT api/<VehicleController>/5
        [HttpPut("{id:int}")]
        public async Task<IActionResult> Put(int id, [FromBody] VehicleViewModel vehicleViewModel)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            var vehicleViewModelResult = await _vehicleAppService.UpdateWithReturn(id, vehicleViewModel);

            return CustomResponse(vehicleViewModelResult);
        }

        // DELETE api/<VehicleController>/5
        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            if (!await _vehicleAppService.Exist(id)) return CustomResponse();

            await _vehicleAppService.Delete(id);

            return CustomResponse();
        }
    }
}
