﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Parking.Application.Interfaces;
using Parking.Application.ViewsModels;
using Parking.CrossCutting.MainController;
using Parking.Domain.Interfaces.Notifications;
using System.Threading.Tasks;

namespace Parking.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class ParkingManagementController : MainController
    {
        private readonly IParkingManagementAppService _parkingManagementAppService;

        public ParkingManagementController(INotifier notifier, IParkingManagementAppService parkingManagementAppService) : base(notifier)
        {
            _parkingManagementAppService = parkingManagementAppService;
        }

        // GET: api/<ParkingManagementController>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var parkingManagements = await _parkingManagementAppService.GetAll();

            if (parkingManagements == null) return NotFound();

            return Ok(parkingManagements);
        }

        // GET api/<ParkingManagementController>/5
        [HttpGet("{id:int}")]
        public async Task<IActionResult> Get(int id)
        {
            if (id == 0) return BadRequest();

            var parkingManagement = await _parkingManagementAppService.GetbyId(id);

            if (parkingManagement == null) return NotFound();

            return Ok(parkingManagement);
        }

        [HttpGet("GetFullById/{id:int}")]
        public async Task<IActionResult> GetFullById(int id)
        {
            if (id == 0) return BadRequest();

            var parkingManagement = await _parkingManagementAppService.GetFullById(id);

            if (parkingManagement == null) return NotFound();

            return Ok(parkingManagement);
        }

        [HttpGet("GetFullByLicensePlate/{licensePlate}")]
        public async Task<IActionResult> GetFullByLicensePlate(string licensePlate)
        {
            if (string.IsNullOrWhiteSpace(licensePlate)) return BadRequest();

            var parkingManagement = await _parkingManagementAppService.GetFullByLicensePlate(licensePlate);

            if (parkingManagement == null) return NotFound();

            return Ok(parkingManagement);
        }

        // POST api/<ParkingManagementController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ParkingManagementViewModel parkingManagementViewModel)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            var parkingManagementViewModelResult = await _parkingManagementAppService.InsertWithReturn(parkingManagementViewModel);

            return CustomResponse(parkingManagementViewModelResult);
        }

        // PUT api/<ParkingManagementController>/5
        [HttpPut]
        public async Task<IActionResult> Put([FromBody] ParkingManagementViewModel parkingManagementViewModel)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            var parkingManagementViewModelResult = await _parkingManagementAppService.UpdateWithReturn(parkingManagementViewModel);

            return CustomResponse(parkingManagementViewModelResult);
        }

        // DELETE api/<ParkingManagementController>/5
        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            if (!await _parkingManagementAppService.Exist(id)) return CustomResponse();

            await _parkingManagementAppService.Delete(id);

            return CustomResponse();
        }
    }
}
