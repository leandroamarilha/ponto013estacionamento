﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Parking.Application.Interfaces;
using Parking.Application.ViewsModels;
using Parking.CrossCutting.MainController;
using Parking.Domain.Interfaces.Notifications;
using System.Threading.Tasks;

namespace Parking.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class ParkingConfigurationController : MainController
    {
        private readonly IParkingConfigurationAppService _parkingConfigurationAppService;

        public ParkingConfigurationController(INotifier notifier, IParkingConfigurationAppService parkingConfigurationAppService) : base(notifier)
        {
            _parkingConfigurationAppService = parkingConfigurationAppService;
        }


        // GET: api/<ParkingConfigurationController>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var parkingConfigurations = await _parkingConfigurationAppService.GetAll();

            if (parkingConfigurations == null) return NotFound();

            return Ok(parkingConfigurations);
        }

        // GET api/<ParkingConfigurationController>/5
        [HttpGet("{id:int}")]
        public async Task<IActionResult> Get(int id)
        {
            if (id == 0) return BadRequest();

            var parkingConfiguration = await _parkingConfigurationAppService.GetbyId(id);

            if (parkingConfiguration == null) return NotFound();

            return Ok(parkingConfiguration);
        }

        [HttpGet("GetFullById/{id:int}")]
        public async Task<IActionResult> GetFullById(int id)
        {
            if (id == 0) return BadRequest();

            var parkingConfiguration = await _parkingConfigurationAppService.GetFullById(id);

            if (parkingConfiguration == null) return NotFound();

            return Ok(parkingConfiguration);
        }

        // POST api/<ParkingConfigurationController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ParkingConfigurationViewModel parkingConfigurationViewModel)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            var parkingConfigurationViewModelResult = await _parkingConfigurationAppService.InsertWithReturn(parkingConfigurationViewModel);

            return CustomResponse(parkingConfigurationViewModelResult);
        }

        // PUT api/<ParkingConfigurationController>/5
        [HttpPut("{id:int}")]
        public async Task<IActionResult> Put(int id, [FromBody] ParkingConfigurationViewModel parkingConfigurationViewModel)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            var parkingConfigurationViewModelResult = await _parkingConfigurationAppService.UpdateWithReturn(id, parkingConfigurationViewModel);

            return CustomResponse(parkingConfigurationViewModelResult);
        }

        // DELETE api/<ParkingConfigurationController>/5
        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            if (!await _parkingConfigurationAppService.Exist(id)) return CustomResponse();

            await _parkingConfigurationAppService.Delete(id);

            return CustomResponse();
        }
    }
}
