﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Parking.Application.Interfaces;
using Parking.Application.ViewsModels;
using Parking.CrossCutting.MainController;
using Parking.Domain.Interfaces.Notifications;
using System.Threading.Tasks;

namespace Parking.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class CompanyController : MainController
    {
        private readonly ICompanyAppService _companyService;

        public CompanyController(INotifier notifier, ICompanyAppService companyService) : base(notifier)
        {
            _companyService = companyService;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var companys = await _companyService.GetAll();

            if (companys == null) return NotFound();

            return Ok(companys);
        }

        [HttpGet("{id:int}")]
        public async Task<IActionResult> Get(int id)
        {
            if (id == 0) return BadRequest();

            var companys = await _companyService.GetbyId(id);

            if (companys == null) return NotFound();

            return Ok(companys);
        }

        [HttpGet("GetFullById/{id:int}")]
        public async Task<IActionResult> GetFullById(int id)
        {
            if (id == 0) return BadRequest();

            var company = await _companyService.GetFullById(id);

            if (company == null) return NotFound();

            return Ok(company);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CompanyViewModel companyViewModel)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            var companyViewModelResult = await _companyService.InsertWithReturn(companyViewModel);

            return CustomResponse(companyViewModelResult);
        }

        [HttpPut("{id:int}")]
        public async Task<IActionResult> Put(int id, [FromBody] CompanyViewModel companyViewModel)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            var companyViewModelResult = await _companyService.UpdateWithReturn(id, companyViewModel);

            return CustomResponse(companyViewModelResult);
        }

        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            if (!await _companyService.Exist(id)) return CustomResponse();

            await _companyService.Delete(id);

            return CustomResponse();
        }
    }
}
