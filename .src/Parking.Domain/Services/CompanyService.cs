﻿using Parking.Domain.Entities;
using Parking.Domain.Entities.Validations;
using Parking.Domain.Interfaces.Notifications;
using Parking.Domain.Interfaces.Repository;
using Parking.Domain.Interfaces.Services;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Parking.Domain.Services
{
    public class CompanyService : BaseService, ICompanyService
    {
        private readonly ICompanyRepository _companyRepository;
        private readonly ICountryRepository _countryRepository;

        public CompanyService(ICompanyRepository companyRepository,
                              ICountryRepository countryRepository,
                              INotifier notifier) : base(notifier)
        {
            _companyRepository = companyRepository;
            _countryRepository = countryRepository;
        }

        public async Task Insert(Company company)
        {
            Validate(company, null, false);

            if (HasNotification()) return;

            await _companyRepository.Insert(company);
        }

        public async Task<Company> InsertWithReturn(Company company)
        {
            Validate(company, null, false);

            if (HasNotification()) return company;

            return await _companyRepository.InsertWithReturn(company);
        }

        public async Task Update(int id, Company company)
        {
            Validate(company, id, true);
            await _companyRepository.Update(company);
        }

        public async Task<Company> UpdateWithReturn(int id, Company company)
        {
            Validate(company, id, true);

            if (HasNotification()) return company;

            return await _companyRepository.UpdateWithReturn(company);
        }

        public async Task Delete(int id)
        {
            await _companyRepository.Delete(id);
        }

        public async Task<bool> Exist(int id)
        {
            var exist = await _companyRepository.Exist(id);

            if (!exist)
            {
                Notify("Registro não encontrado.");
                return exist;
            }

            return exist;
        }

        public async Task<Company> GetById(int id)
        {
            return await _companyRepository.GetById(id);
        }

        public async Task<List<Company>> GetAll()
        {
            return await _companyRepository.GetAll();
        }

        public async Task<Company> GetFullById(int id)
        {
            return await _companyRepository.GetFullById(id);
        }

        public void Dispose()
        {
            _companyRepository.Dispose();
        }

        private void Validate(Company company, int? idUpdate, bool isUpdate = false)
        {
            RunValidation(new CompanyValidation(), company);
            RunValidation(new CompanyConfigurationValidation(), company?.CompanyConfigurations);
            RunValidation(new PhoneValidation(), company?.Phones);
            RunValidation(new AddressValidation(), company?.Address);

            if (isUpdate)
            {
                if (idUpdate != company.Id)
                {
                    Notify("O 'Id' da Empresa informado não é o mesmo que foi passado nos dados para atualização.");
                }

                RunValidation(new CountryValidation(), company?.Address?.Country);
            }

            if (!isUpdate && _companyRepository.Search(f => f.CNPJ == company.CNPJ).Result.Any())
            {
                Notify("Já existe uma empresa cadastrada com o CNPJ informado.");
            }

            if (!_countryRepository.Search(x => x.Id == company.Address.CountryId).Result.Any())
            {
                Notify("O countryId informado não existe.");
            }
        }

        public async Task<IEnumerable<Company>> Search(Expression<System.Func<Company, bool>> predicate)
        {
            return await _companyRepository.Search(predicate);
        }
    }
}
