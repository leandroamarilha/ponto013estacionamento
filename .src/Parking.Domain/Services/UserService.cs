﻿using Parking.Domain.Entities;
using Parking.Domain.Entities.Validations;
using Parking.Domain.Interfaces.Notifications;
using Parking.Domain.Interfaces.Repository;
using Parking.Domain.Interfaces.Services;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Parking.CrossCutting.Security.Encryption;
using System.Linq.Expressions;

namespace Parking.Domain.Services
{
    public class UserService : BaseService, IUserService
    {
        private readonly IUserRepository _userRepository;
        public UserService(IUserRepository userRepository,
                           INotifier notifier) : base(notifier)
        {
            _userRepository = userRepository;
        }

        #region Login

        public async Task<User> VerifyLogin(User user)
        {
            ValidateLogin(user);

            if (HasNotification()) return null;

            var userLogin = await _userRepository.VerifyLogin(user);

            if (userLogin == null)
            {
                Notify("Usuário ou Senha inválido.");
                return null;
            }

            if (userLogin.Password != UtilEncryption.HashHmac(user.UserName + "!@#$%QWE", user.Password))
            {
                Notify("Usuário ou Senha inválido.");
                return null;
            }

            return userLogin;
        }

        #endregion

        public async Task Delete(int id)
        {
            await _userRepository.Delete(id);
        }

        public void Dispose()
        {
            _userRepository.Dispose();
        }

        public async Task<bool> Exist(int id)
        {
            var exist = await _userRepository.Exist(id);

            if (!exist)
            {
                Notify("Registro não encontrado.");
                return exist;
            }

            return exist;
        }

        public async Task<List<User>> GetAll()
        {
            return await _userRepository.GetAll();
        }

        public async Task<User> GetById(int id)
        {
            return await _userRepository.GetById(id);
        }

        public async Task<User> GetFullById(int id)
        {
            return await _userRepository.GetFullById(id);
        }

        public async Task Insert(User user)
        {
            Validate(user, null, false);

            if (HasNotification()) return;

            await _userRepository.Insert(user);
        }

        public async Task<User> InsertWithReturn(User user)
        {
            Validate(user, null, false);

            if (HasNotification()) return user;

            return await _userRepository.InsertWithReturn(user);
        }

        public async Task Update(int id, User user)
        {
            Validate(user, id, true);
            await _userRepository.Update(user);
        }

        public async Task<User> UpdateWithReturn(int id, User user)
        {
            Validate(user, id, true);

            if (HasNotification()) return user;

            return await _userRepository.UpdateWithReturn(user);
        }

        private void Validate(User user, int? idUpdate, bool isUpdate = false)
        {
            RunValidation(new UserValidation(), user);

            if (isUpdate)
            {
                if (idUpdate != user.Id)
                {
                    Notify("O 'Id' do Usuário informado não é o mesmo que foi passado nos dados para atualização.");
                }
            }
        }

        private void ValidateLogin(User user)
        {
            RunValidation(new UserLoginValidation(), user);
        }

        public async Task<IEnumerable<User>> Search(Expression<System.Func<User, bool>> predicate)
        {
            return await _userRepository.Search(predicate);
        }
    }
}
