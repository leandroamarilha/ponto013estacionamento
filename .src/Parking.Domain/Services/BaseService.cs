﻿using FluentValidation;
using FluentValidation.Results;
using Parking.Domain.Entities;
using Parking.Domain.Interfaces.Notifications;
using Parking.Domain.Interfaces.Services;
using Parking.Domain.Notifications;
using System.Collections.Generic;

namespace Parking.Domain.Services
{
    public abstract class BaseService
    {
        private readonly INotifier _notifier;

        protected BaseService(INotifier notifier)
        {
            _notifier = notifier;
        }

        protected void Notify(ValidationResult validationResult)
        {
            foreach (var error in validationResult.Errors)
            {
                Notify(error.ErrorMessage);
            }
        }

        protected void Notify(string mensagem)
        {
            _notifier.Handle(new Notification(mensagem));
        }

        protected bool RunValidation<TV, TE>(TV validation, TE entity) where TV : AbstractValidator<TE> where TE : EntityBase
        {
            if (entity == null)
            {
                Notify("Um item obrigatório não foi passado.");
                return false;
            }

            var validator = validation.Validate(entity);

            if (validator.IsValid) return true;

            Notify(validator);

            return false;
        }

        protected bool RunValidation<TV, TE>(TV validation, ICollection<TE> entitys) where TV : AbstractValidator<TE> where TE : EntityBase
        {
            if (entitys == null)
            {
                Notify("Um item obrigatório não foi passado.");
                return false;
            }

            foreach (var entity in entitys)
            {
                var validator = validation.Validate(entity);

                if (!validator.IsValid) Notify(validator);
            }

            return !_notifier.HasNotification();
        }

        public bool HasNotification()
        {
            return _notifier.HasNotification();
        }
    }
}
