﻿using Parking.Domain.Entities;
using Parking.Domain.Entities.Validations;
using Parking.Domain.Interfaces.Notifications;
using Parking.Domain.Interfaces.Repository;
using Parking.Domain.Interfaces.Services;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Parking.CrossCutting.Security.Encryption;
using System.Linq.Expressions;
using System;

namespace Parking.Domain.Services
{
    public class VehicleService : BaseService, IVehicleService
    {
        private readonly IVehicleRepository _vehicleRepository;

        public VehicleService(IVehicleRepository vehicleRepository,
                              INotifier notifier) : base(notifier)
        {
            _vehicleRepository = vehicleRepository;
        }

        public async Task Delete(int id)
        {
            await _vehicleRepository.Delete(id);
        }

        public void Dispose()
        {
            _vehicleRepository.Dispose();
        }

        public async Task<bool> Exist(int id)
        {
            var exist = await _vehicleRepository.Exist(id);

            if (!exist)
            {
                Notify("Registro não encontrado.");
                return exist;
            }

            return exist;
        }

        public async Task<bool> ExistByLicensePlate(string licensePlate)
        {
            var exist = await _vehicleRepository.ExistByLicensePlate(licensePlate);

            if (!exist)
            {
                Notify("Registro não encontrado.");
                return exist;
            }

            return exist;
        }

        public async Task<List<Vehicle>> GetAll()
        {
            return await _vehicleRepository.GetAll();
        }

        public async Task<Vehicle> GetById(int id)
        {
            return await _vehicleRepository.GetById(id);
        }

        public async Task<Vehicle> GetFullById(int id)
        {
            return await _vehicleRepository.GetFullById(id);
        }

        public async Task<Vehicle> GetVehicleByLicensePlate(string licensePlate)
        {
            return await _vehicleRepository.GetVehicleByLicensePlate(licensePlate);
        }

        public async Task Insert(Vehicle vehicle)
        {
            await Validate(vehicle, null, false);

            if (HasNotification()) return;

            await _vehicleRepository.Insert(vehicle);
        }

        public async Task<Vehicle> InsertWithReturn(Vehicle vehicle)
        {
            await Validate(vehicle, null, false);

            if (HasNotification()) return vehicle;

            return await _vehicleRepository.InsertWithReturn(vehicle);
        }

        public async Task<IEnumerable<Vehicle>> Search(Expression<Func<Vehicle, bool>> predicate)
        {
            return await _vehicleRepository.Search(predicate);
        }

        public async Task Update(int id, Vehicle vehicle)
        {
            await Validate(vehicle, id, true);
            await _vehicleRepository.Update(vehicle);
        }

        public async Task<Vehicle> UpdateWithReturn(int id, Vehicle vehicle)
        {
            await Validate(vehicle, id, true);

            if (HasNotification()) return vehicle;

            return await _vehicleRepository.UpdateWithReturn(vehicle);
        }

        private async Task Validate(Vehicle vehicle, int? idUpdate, bool isUpdate = false)
        {
            RunValidation(new VehicleValidation(), vehicle);

            if (isUpdate)
            {
                if (idUpdate != vehicle.Id)
                {
                    Notify("O 'Id' do Veículo informado não é o mesmo que foi passado nos dados para atualização.");
                }
            }
            else
            {
                var plateExist = await _vehicleRepository.ExistByLicensePlate(vehicle.LicensePlate);
                if (plateExist)
                {
                    Notify($"Já existe um veículo cadastrado no sistema para a placa {vehicle.LicensePlate}.");
                }
            }
        }
    }
}
