﻿using Parking.Domain.Entities;
using Parking.Domain.Entities.Validations;
using Parking.Domain.Interfaces.Notifications;
using Parking.Domain.Interfaces.Repository;
using Parking.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Parking.Domain.Services
{
    public class ParkingManagementService : BaseService, IParkingManagementService
    {
        private readonly IParkingManagementRepository _parkingManagementRepository;
        private readonly IVehicleRepository _vehicleRepository;

        public ParkingManagementService(IParkingManagementRepository parkingManagementRepository,
                                        IVehicleRepository vehicleRepository,
                                        INotifier notifier) : base(notifier)
        {
            _parkingManagementRepository = parkingManagementRepository;
            _vehicleRepository = vehicleRepository;
        }

        public async Task Delete(int id)
        {
            await _parkingManagementRepository.Delete(id);
        }

        public void Dispose()
        {
            _parkingManagementRepository.Dispose();
        }

        public async Task<bool> Exist(int id)
        {
            var exist = await _parkingManagementRepository.Exist(id);

            if (!exist)
            {
                Notify("Registro não encontrado.");
                return exist;
            }

            return exist;
        }

        public async Task<List<ParkingManagement>> GetAll()
        {
            return await _parkingManagementRepository.GetAll();
        }

        public async Task<ParkingManagement> GetById(int id)
        {
            return await _parkingManagementRepository.GetById(id);
        }

        public async Task<ParkingManagement> GetFullById(int id)
        {
            return await _parkingManagementRepository.GetFullById(id);
        }

        public async Task<ParkingManagement> GetFullByLicensePlate(string licensePlate)
        {
            return await _parkingManagementRepository.GetFullByLicensePlate(licensePlate);
        }

        public async Task Insert(ParkingManagement parkingManagement, string licensePlate)
        {
            if (string.IsNullOrWhiteSpace(licensePlate))
            {
                Notify("Favor informar a placa do veículo.");
                return;
            }

            var vehicle = await _vehicleRepository.GetVehicleByLicensePlate(parkingManagement.Vehicle.LicensePlate);
            if (vehicle == null)
            {
                Notify("Não foi encontrado nenhum cadastro de veículo para a placa informada.");
                return;
            }

            parkingManagement.DateTimeEntry = DateTime.Now;
            parkingManagement.VehicleId = vehicle.Id;

            await ValidateVehicleEntry(parkingManagement, vehicle);

            if (HasNotification()) return;

            await _parkingManagementRepository.Insert(parkingManagement);
        }

        public async Task<ParkingManagement> InsertWithReturn(ParkingManagement parkingManagement, string licensePlate)
        {
            if (string.IsNullOrWhiteSpace(licensePlate))
            {
                Notify("Favor informar a placa do veículo.");
                return parkingManagement;
            }

            var vehicle = await _vehicleRepository.GetVehicleByLicensePlate(licensePlate);
            if (vehicle == null)
            {
                Notify("Não foi encontrado nenhum cadastro de veículo para a placa informada.");
                return parkingManagement;
            }

            parkingManagement.DateTimeEntry = DateTime.Now;
            parkingManagement.VehicleId = vehicle.Id;

            await ValidateVehicleEntry(parkingManagement, vehicle);

            if (HasNotification()) return parkingManagement;

            return await _parkingManagementRepository.InsertWithReturn(parkingManagement);
        }

        public Task<ParkingManagement> InsertWithReturn(ParkingManagement entity)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<ParkingManagement>> Search(Expression<Func<ParkingManagement, bool>> predicate)
        {
            return await _parkingManagementRepository.Search(predicate);
        }

        public async Task Update(ParkingManagement parkingManagement, string licensePlate)
        {
            if (string.IsNullOrWhiteSpace(licensePlate))
            {
                Notify("Favor informar a placa do veículo.");
                return;
            }

            var vehicle = await _vehicleRepository.GetVehicleByLicensePlate(licensePlate);
            if (vehicle == null)
            {
                Notify("Não foi encontrado nenhum cadastro de veículo para a placa informada.");
                return;
            }

            parkingManagement.DateTimeExit = DateTime.Now;
            parkingManagement.VehicleId = vehicle.Id;

            await ValidateVehicleExit(parkingManagement, vehicle);

            if (HasNotification()) return;

            await _parkingManagementRepository.Update(parkingManagement);
        }

        public async Task<ParkingManagement> UpdateWithReturn(ParkingManagement parkingManagement, string licensePlate)
        {
            if (string.IsNullOrWhiteSpace(licensePlate))
            {
                Notify("Favor informar a placa do veículo.");
                return parkingManagement;
            }

            var vehicle = await _vehicleRepository.GetVehicleByLicensePlate(licensePlate);
            if (vehicle == null)
            {
                Notify("Não foi encontrado nenhum cadastro de veículo para a placa informada.");
                return parkingManagement;
            }

            var parkingManagementDb = await _parkingManagementRepository.GetFullByLicensePlate(licensePlate);
            if (parkingManagementDb == null)
            {
                Notify($"O veículo de placa '{vehicle.LicensePlate}' não encontra-se estacionado em nosso pátio.");
                return parkingManagement;
            }

            parkingManagementDb.DateTimeExit = DateTime.Now;

            await ValidateVehicleExit(parkingManagementDb, vehicle);

            if (HasNotification()) return parkingManagement;

            return await _parkingManagementRepository.UpdateWithReturn(parkingManagementDb);
        }

        public async Task<int> GetParkingAvailable(int companyId, int vehicleTypeId)
        {
            return await _parkingManagementRepository.GetParkingAvailable(companyId, vehicleTypeId);
        }

        public Task Insert(ParkingManagement entity)
        {
            throw new NotImplementedException();
        }

        public Task Update(int id, ParkingManagement entity)
        {
            throw new NotImplementedException();
        }

        public Task<ParkingManagement> UpdateWithReturn(int id, ParkingManagement entity)
        {
            throw new NotImplementedException();
        }

        private async Task ValidateVehicleEntry(ParkingManagement parkingManagement, Vehicle vehicle)
        {
            RunValidation(new ParkingManagementValidation(), parkingManagement);

            if (parkingManagement.VehicleId == default) return;

            var vehicleInTheParking = await _parkingManagementRepository.VehicleInTheParking(vehicle.LicensePlate);
            if (vehicleInTheParking)
            {
                Notify($"O veículo de placa '{vehicle.LicensePlate}' encontra-se estacionado em nosso pátio.");
            }

            var totalParkingAvailable = await _parkingManagementRepository.GetParkingAvailable(vehicle.CompanyId, vehicle.VehicleTypeId);
            if (totalParkingAvailable <= 0)
            {
                Notify($"Todas as vagas disponíveis em nosso pátio para a empresa {vehicle?.Company?.Name} estão ocupadas.");
            }
        }

        private async Task ValidateVehicleExit(ParkingManagement parkingManagement, Vehicle vehicle)
        {
            RunValidation(new ParkingManagementValidation(), parkingManagement);

            if (parkingManagement.VehicleId == default) return;

            var vehicleInTheParking = await _parkingManagementRepository.VehicleInTheParking(vehicle.LicensePlate);
            if (!vehicleInTheParking)
            {
                Notify($"O veículo de placa '{vehicle.LicensePlate}' não encontra-se estacionado em nosso pátio.");
            }
        }
    }
}
