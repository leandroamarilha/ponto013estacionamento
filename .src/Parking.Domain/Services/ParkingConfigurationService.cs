﻿using Parking.Domain.Entities;
using Parking.Domain.Entities.Validations;
using Parking.Domain.Interfaces.Notifications;
using Parking.Domain.Interfaces.Repository;
using Parking.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Parking.Domain.Services
{
    public class ParkingConfigurationService : BaseService, IParkingConfigurationService
    {
        private readonly IParkingConfigurationRepository _parkingConfigurationRepository;

        public ParkingConfigurationService(IParkingConfigurationRepository parkingConfigurationRepository,
                              INotifier notifier) : base(notifier)
        {
            _parkingConfigurationRepository = parkingConfigurationRepository;
        }

        public async Task Delete(int id)
        {
            await _parkingConfigurationRepository.Delete(id);
        }

        public void Dispose()
        {
            _parkingConfigurationRepository.Dispose();
        }

        public async Task<bool> Exist(int id)
        {
            var exist = await _parkingConfigurationRepository.Exist(id);

            if (!exist)
            {
                Notify("Registro não encontrado.");
                return exist;
            }

            return exist;
        }

        public async Task<List<ParkingConfiguration>> GetAll()
        {
            return await _parkingConfigurationRepository.GetAll();
        }

        public async Task<ParkingConfiguration> GetById(int id)
        {
            return await _parkingConfigurationRepository.GetById(id);
        }

        public async Task<ParkingConfiguration> GetFullById(int id)
        {
            return await _parkingConfigurationRepository.GetFullById(id);
        }

        public async Task Insert(ParkingConfiguration parkingConfiguration)
        {
            Validate(parkingConfiguration, null, false);

            if (HasNotification()) return;

            await _parkingConfigurationRepository.Insert(parkingConfiguration);
        }

        public async Task<ParkingConfiguration> InsertWithReturn(ParkingConfiguration parkingConfiguration)
        {
            Validate(parkingConfiguration, null, false);

            if (HasNotification()) return parkingConfiguration;

            return await _parkingConfigurationRepository.InsertWithReturn(parkingConfiguration);
        }

        public async Task<IEnumerable<ParkingConfiguration>> Search(Expression<Func<ParkingConfiguration, bool>> predicate)
        {
            return await _parkingConfigurationRepository.Search(predicate);
        }

        public async Task Update(int id, ParkingConfiguration parkingConfiguration)
        {
            Validate(parkingConfiguration, id, true);
            await _parkingConfigurationRepository.Update(parkingConfiguration);
        }

        public async Task<ParkingConfiguration> UpdateWithReturn(int id, ParkingConfiguration parkingConfiguration)
        {
            Validate(parkingConfiguration, id, true);

            if (HasNotification()) return parkingConfiguration;

            return await _parkingConfigurationRepository.UpdateWithReturn(parkingConfiguration);
        }

        public async Task<int> GetParkingTotal()
        {
            return await _parkingConfigurationRepository.GetParkingTotal();
        }

        private void Validate(ParkingConfiguration parkingConfiguration, int? idUpdate, bool isUpdate = false)
        {
            RunValidation(new ParkingConfigurationValidation(), parkingConfiguration);

            if (isUpdate)
            {
                if (idUpdate != parkingConfiguration.Id)
                {
                    Notify("O 'Id' do Veículo informado não é o mesmo que foi passado nos dados para atualização.");
                }
            }
        }
    }
}
