﻿using System;

namespace Parking.Domain.Interfaces.Entities
{
    public interface IVehicleType
    {
        int Id { get; set; }
        string Type { get; set; }

        DateTime DateCreated { get; set; }
    }
}
