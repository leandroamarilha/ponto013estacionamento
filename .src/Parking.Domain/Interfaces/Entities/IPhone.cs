﻿using Parking.Domain.Entities;
using System;

namespace Parking.Domain.Interfaces.Entities
{
    public interface IPhone
    {
        int Id { get; set; }
        string Number { get; set; }

        int CompanyId { get; set; }
        Company Company { get; set; }

        int PhoneTypeId { get; set; }
        PhoneType PhoneType { get; set; }

        DateTime DateCreated { get; set; }
    }
}
