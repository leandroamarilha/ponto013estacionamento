﻿using Parking.Domain.Entities;
using System;
using System.Collections.Generic;

namespace Parking.Domain.Interfaces.Entities
{
    public interface ICompany
    {
        int Id { get; set; }
        string Name { get; set; }
        string CNPJ { get; set; }

        Address Address { get; set; }
        ICollection<Phone> Phones { get; set; }

        ICollection<Vehicle> Vehicles { get; set; }

        DateTime DateCreated { get; set; }
    }
}
