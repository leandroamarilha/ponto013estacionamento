﻿using System;

namespace Parking.Domain.Interfaces.Entities
{
    public interface IPhoneType
    {
        int Id { get; set; }
        string Type { get; set; }

        DateTime DateCreated { get; set; }
    }
}
