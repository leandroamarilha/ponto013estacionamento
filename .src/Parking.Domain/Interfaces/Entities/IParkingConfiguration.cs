﻿using System;

namespace Parking.Domain.Interfaces.Entities
{
    public interface IParkingConfiguration
    {
        int Id { get; set; }
        int TotalParking { get; set; }

        DateTime DateCreated { get; set; }
    }
}
