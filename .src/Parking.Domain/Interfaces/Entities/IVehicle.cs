﻿using Parking.Domain.Entities;
using System;

namespace Parking.Domain.Interfaces.Entities
{
    public interface IVehicle
    {
        int Id { get; set; }
        string Brand { get; set; }
        string Model { get; set; }
        string Color { get; set; }
        string LicensePlate { get; set; }

        int VehicleTypeId { get; set; }
        VehicleType VehicleType { get; set; }

        int CompanyId { get; set; }
        Company Company { get; set; }

        DateTime DateCreated { get; set; }
    }
}
