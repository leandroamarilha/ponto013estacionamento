﻿using System;

namespace Parking.Domain.Interfaces.Entities
{
    public interface IUser
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        DateTime DateCreated { get; set; }
    }
}
