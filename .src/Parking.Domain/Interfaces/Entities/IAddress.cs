﻿using Parking.Domain.Entities;
using System;

namespace Parking.Domain.Interfaces.Entities
{
    public interface IAddress
    {
        int Id { get; set; }
        string Name { get; set; }
        string Number { get; set; }
        string Complement { get; set; }
        string City { get; set; }
        string State { get; set; }
        string ZipCode { get; set; }

        int CompanyId { get; set; }
        Company Company { get; set; }

        Country Country { get; set; }
        int CountryId { get; set; }

        DateTime DateCreated { get; set; }
    }
}
