﻿using Parking.Domain.Entities;
using System;

namespace Parking.Domain.Interfaces.Entities
{
    public interface IParkingManagement
    {
        int Id { get; set; }
        DateTime DateTimeEntry { get; set; }
        DateTime? DateTimeExit { get; set; }

        int VehicleId { get; set; }
        Vehicle Vehicle { get; set; }

        DateTime DateCreated { get; set; }
    }
}
