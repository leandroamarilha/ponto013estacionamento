﻿using Parking.Domain.Entities;
using System;

namespace Parking.Domain.Interfaces.Entities
{
    public interface ICompanyConfiguration
    {
        int ParkingGarageTotal { get; set; }

        int VehicleTypeId { get; set; }
        VehicleType VehicleType { get; set; }

        int CompanyId { get; set; }
        Company Company { get; set; }

        DateTime DateCreated { get; set; }
    }
}
