﻿using System;

namespace Parking.Domain.Interfaces.Entities
{
    public interface ICountry
    {
        int Id { get; set; }
        string Name { get; set; }
        string Code { get; set; }

        DateTime DateCreated { get; set; }
    }
}
