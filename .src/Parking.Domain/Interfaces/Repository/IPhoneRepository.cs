﻿using Parking.Domain.Entities;
using System.Threading.Tasks;

namespace Parking.Domain.Interfaces.Repository
{
    public interface IPhoneRepository : IRepository<Phone>
    {
        Task<Phone> GetFullById(int id);
    }
}
