﻿using Parking.Domain.Entities;
using System.Threading.Tasks;

namespace Parking.Domain.Interfaces.Repository
{
    public interface IPhoneTypeRepository : IRepository<PhoneType>
    {
        Task<PhoneType> GetFullById(int id);
    }
}
