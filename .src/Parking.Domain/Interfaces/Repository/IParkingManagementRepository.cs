﻿using Parking.Domain.Entities;
using System.Threading.Tasks;

namespace Parking.Domain.Interfaces.Repository
{
    public interface IParkingManagementRepository : IRepository<ParkingManagement>
    {
        Task<bool> VehicleInTheParking(string licensePlate);
        Task<ParkingManagement> GetFullById(int id);
        Task<ParkingManagement> GetFullByLicensePlate(string licensePlate);
        Task<int> GetParkingAvailable(int companyId, int vehicleTypeId);
    }
}