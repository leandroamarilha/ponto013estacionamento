﻿using Parking.Domain.Entities;
using System.Threading.Tasks;

namespace Parking.Domain.Interfaces.Repository
{
    public interface IAddressRepository : IRepository<Address>
    {
        Task<Address> GetFullById(int id);
    }
}
