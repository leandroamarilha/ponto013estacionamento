﻿using Parking.Domain.Entities;
using System.Threading.Tasks;

namespace Parking.Domain.Interfaces.Repository
{
    public interface ICompanyConfigurationRepository : IRepository<CompanyConfiguration>
    {
        Task<CompanyConfiguration> GetFullById(int id);
    }
}
