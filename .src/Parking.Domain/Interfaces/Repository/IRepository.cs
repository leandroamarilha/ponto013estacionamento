﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Parking.Domain.Interfaces.Repository
{
    public interface IRepository<TEntity> : IDisposable where TEntity : class
    {
        Task Insert(TEntity entity);
        Task<TEntity> InsertWithReturn(TEntity entity);
        Task Update(TEntity entity);
        Task<TEntity> UpdateWithReturn(TEntity entity);
        Task Delete(int id);
        Task<bool> Exist(int id);
        Task<TEntity> GetById(int id);
        Task<List<TEntity>> GetAll();
        Task<IEnumerable<TEntity>> Search(Expression<Func<TEntity, bool>> predicate);

        Task<int> SaveChanges();
    }
}
