﻿using Parking.Domain.Entities;
using System.Threading.Tasks;

namespace Parking.Domain.Interfaces.Repository
{
    public interface IParkingConfigurationRepository : IRepository<ParkingConfiguration>
    {
        Task<ParkingConfiguration> GetFullById(int id);
        Task<int> GetParkingTotal();
    }
}
