﻿using Parking.Domain.Entities;
using System.Threading.Tasks;

namespace Parking.Domain.Interfaces.Repository
{
    public interface IVehicleTypeRepository : IRepository<VehicleType>
    {
        Task<VehicleType> GetFullById(int id);
    }
}
