﻿using Parking.Domain.Entities;
using System.Threading.Tasks;

namespace Parking.Domain.Interfaces.Repository
{
    public interface ICompanyRepository : IRepository<Company>
    {
        Task<Company> GetFullById(int id);
    }
}
