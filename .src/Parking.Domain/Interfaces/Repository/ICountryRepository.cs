﻿using Parking.Domain.Entities;
using System.Threading.Tasks;

namespace Parking.Domain.Interfaces.Repository
{
    public interface ICountryRepository : IRepository<Country>
    {
        Task<Country> GetFullById(int id);
    }
}
