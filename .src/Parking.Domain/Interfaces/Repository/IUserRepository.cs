﻿using Parking.Domain.Entities;
using System.Threading.Tasks;

namespace Parking.Domain.Interfaces.Repository
{
    public interface IUserRepository : IRepository<User>
    {
        Task<User> VerifyLogin(User user);
        Task<User> GetFullById(int id);
    }
}
