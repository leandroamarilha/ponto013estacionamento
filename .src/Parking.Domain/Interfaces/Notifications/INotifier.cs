﻿using Parking.Domain.Notifications;
using System.Collections.Generic;

namespace Parking.Domain.Interfaces.Notifications
{
    public interface INotifier
    {
        bool HasNotification();
        List<Notification> GetNotifications();
        void Handle(Notification notification);
    }
}
