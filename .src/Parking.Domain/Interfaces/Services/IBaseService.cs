﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Parking.Domain.Interfaces.Services
{
    public interface IBaseService<TEntity> : IDisposable where TEntity : class
    {
        Task Insert(TEntity entity);
        Task<TEntity> InsertWithReturn(TEntity entity);
        Task Update(int id, TEntity entity);
        Task<TEntity> UpdateWithReturn(int id, TEntity entity);
        Task Delete(int id);
        Task<bool> Exist(int id);
        Task<TEntity> GetById(int id);
        Task<List<TEntity>> GetAll();
        Task<IEnumerable<TEntity>> Search(Expression<Func<TEntity, bool>> predicate);
    }
}
