﻿using Parking.Domain.Entities;
using System.Threading.Tasks;

namespace Parking.Domain.Interfaces.Services
{
    public interface IParkingManagementService : IBaseService<ParkingManagement>
    {
        Task<ParkingManagement> GetFullById(int id);
        Task<ParkingManagement> GetFullByLicensePlate(string licensePlate);
        Task<int> GetParkingAvailable(int companyId, int vehicleTypeId);
        Task<ParkingManagement> InsertWithReturn(ParkingManagement parkingManagement, string licensePlate);
        Task Insert(ParkingManagement parkingManagement, string licensePlate);
        Task Update(ParkingManagement parkingManagement, string licensePlate);
        Task<ParkingManagement> UpdateWithReturn(ParkingManagement parkingManagement, string licensePlate);
    }
}
