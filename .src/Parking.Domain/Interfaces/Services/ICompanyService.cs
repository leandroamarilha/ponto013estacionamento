﻿using Parking.Domain.Entities;
using System.Threading.Tasks;

namespace Parking.Domain.Interfaces.Services
{
    public interface ICompanyService : IBaseService<Company>
    {
        Task<Company> GetFullById(int id);
    }
}
