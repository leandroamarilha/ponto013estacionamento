﻿using Parking.Domain.Entities;
using System.Threading.Tasks;

namespace Parking.Domain.Interfaces.Services
{
    public interface IParkingConfigurationService : IBaseService<ParkingConfiguration>
    {
        Task<ParkingConfiguration> GetFullById(int id);
        Task<int> GetParkingTotal();
    }
}
