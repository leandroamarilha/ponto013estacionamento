﻿using Parking.Domain.Entities;
using System.Threading.Tasks;

namespace Parking.Domain.Interfaces.Services
{
    public interface IVehicleService : IBaseService<Vehicle>
    {
        Task<Vehicle> GetFullById(int id);
        Task<Vehicle> GetVehicleByLicensePlate(string licensePlate);
        Task<bool> ExistByLicensePlate(string licensePlate);
    }
}
