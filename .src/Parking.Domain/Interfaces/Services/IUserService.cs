﻿using Parking.Domain.Entities;
using System.Threading.Tasks;

namespace Parking.Domain.Interfaces.Services
{
    public interface IUserService : IBaseService<User>
    {
        Task<User> VerifyLogin(User user);
        Task<User> GetFullById(int id);
    }
}
