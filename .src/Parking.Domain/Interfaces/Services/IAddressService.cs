﻿using Parking.Domain.Entities;
using System.Threading.Tasks;

namespace Parking.Domain.Interfaces.Services
{
    public interface IAddressService : IBaseService<Address>
    {
        Task<Address> GetFullById(int id);
    }
}
