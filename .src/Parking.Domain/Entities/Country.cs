﻿using Parking.Domain.Interfaces.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Parking.Domain.Entities
{
    public class Country : EntityBase, ICountry
    {
        public const int NameMinLength = 2;
        public const int NameMaxLength = 200;

        public const int CodeMinLength = 2;
        public const int CodeMaxLength = 2;

        public Country()
        {
            DateCreated = DateTime.Now;
        }

        public string Name { get; set; }
        public string Code { get; set; }
    }
}
