﻿using Parking.Domain.Interfaces.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Parking.Domain.Entities
{
    public class Company : EntityBase, ICompany
    {
        public const int NameMinLength = 2;
        public const int NameMaxLength = 200;

        public const int CNPJMinLength = 14;
        public const int CNPJMaxLength = 14;

        public Company()
        {
            Phones = new HashSet<Phone>();
            CompanyConfigurations = new HashSet<CompanyConfiguration>();
            DateCreated = DateTime.Now;
        }

        public string Name { get; set; }
        public string CNPJ { get; set; }

        public virtual Address Address { get; set; }

        public virtual ICollection<Phone> Phones { get; set; }

        public virtual ICollection<Vehicle> Vehicles { get; set; }

        public virtual ICollection<CompanyConfiguration> CompanyConfigurations { get; set; }
    }
}
