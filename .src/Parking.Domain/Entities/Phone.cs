﻿using Parking.Domain.Interfaces.Entities;
using System;

namespace Parking.Domain.Entities
{
    public class Phone : EntityBase, IPhone
    {
        public const int NumberMaxLength = 14;

        public Phone()
        {
            DateCreated = DateTime.Now;
        }

        public string Number { get; set; }

        public int CompanyId { get; set; }
        public virtual Company Company { get; set; }

        public int PhoneTypeId { get; set; }
        public virtual PhoneType PhoneType { get; set; }
    }
}
