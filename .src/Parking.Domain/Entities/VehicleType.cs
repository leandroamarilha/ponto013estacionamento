﻿using Parking.Domain.Interfaces.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Parking.Domain.Entities
{
    public class VehicleType : EntityBase, IVehicleType
    {
        public VehicleType()
        {
            DateCreated = DateTime.Now;
        }

        public string Type { get; set; }
    }
}
