﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Parking.Domain.Entities
{
    public abstract class EntityBase
    {
        public EntityBase()
        {
            DateCreated = DateTime.Now;
        }

        public int Id { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
