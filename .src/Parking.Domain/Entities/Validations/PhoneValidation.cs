﻿using FluentValidation;

namespace Parking.Domain.Entities.Validations
{
    public class PhoneValidation : AbstractValidator<Phone>
    {
        public PhoneValidation()
        {
            RuleFor(x => x.Number)
                .NotEmpty().WithMessage("O campo '{PropertyName}' precisa ser fornecido.")
                .NotNull().WithMessage("O campo '{PropertyName}' não pode ser nulo.")
                .Length(Phone.NumberMaxLength).WithMessage("O campo '{PropertyName}' precisa ter {MaxLength} caracteres.");

            RuleFor(x => x.PhoneTypeId)
                .NotEmpty().WithMessage("O campo '{PropertyName}' precisa ser fornecido.")
                .NotNull().WithMessage("O campo '{PropertyName}' não pode ser nulo.");
        }
    }
}
