﻿using FluentValidation;

namespace Parking.Domain.Entities.Validations
{
    public class CompanyConfigurationValidation : AbstractValidator<CompanyConfiguration>
    {
        public CompanyConfigurationValidation()
        {
            RuleFor(x => x.ParkingGarageTotal)
                .NotEmpty().WithMessage("O campo '{PropertyName}' precisa ser fornecido.")
                .NotNull().WithMessage("O campo '{PropertyName}' não pode ser nulo.")
                .GreaterThan(0).WithMessage("O campo '{PropertyName}' precisa ser maior que {ComparisonValue}.");

            RuleFor(x => x.VehicleTypeId)
                .NotEmpty().WithMessage("O campo '{PropertyName}' precisa ser fornecido.")
                .NotNull().WithMessage("O campo '{PropertyName}' não pode ser nulo.")
                .GreaterThan(0).WithMessage("O campo '{PropertyName}' precisa ser maior que {ComparisonValue}.");
        }
    }
}
