﻿using FluentValidation;

namespace Parking.Domain.Entities.Validations
{
    public class PhoneTypeValidation : AbstractValidator<PhoneType>
    {
        public PhoneTypeValidation()
        {
            RuleFor(x => x.Type)
                .NotEmpty().WithMessage("O campo '{PropertyName}' precisa ser fornecido.")
                .NotNull().WithMessage("O campo '{PropertyName}' não pode ser nulo.")
                .Length(PhoneType.TypeMinLength, PhoneType.TypeMaxLength).WithMessage("O campo '{PropertyName}' precisa ter entre {MinLength} e {MaxLength} caracteres.");
        }
    }
}
