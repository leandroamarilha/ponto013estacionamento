﻿using FluentValidation;

namespace Parking.Domain.Entities.Validations
{
    public class UserValidation : AbstractValidator<User>
    {
        public UserValidation()
        {
            RuleFor(x => x.UserName)
                .NotEmpty().WithMessage("O campo '{PropertyName}' precisa ser fornecido.")
                .NotNull().WithMessage("O campo '{PropertyName}' não pode ser nulo.")
                .Length(User.UserNameMinLength, User.UserNameMaxLength).WithMessage("O campo '{PropertyName}' precisa ter entre {MinLength} e {MaxLength} caracteres.");

            RuleFor(x => x.Password)
                .NotEmpty().WithMessage("O campo '{PropertyName}' precisa ser fornecido.")
                .NotNull().WithMessage("O campo '{PropertyName}' não pode ser nulo.")
                .Length(User.PasswordNameMinLength, User.PasswordNameMaxLength).WithMessage("O campo '{PropertyName}' precisa ter entre {MinLength} e {MaxLength} caracteres.");
        }
    }
}
