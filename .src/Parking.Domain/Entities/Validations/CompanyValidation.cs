﻿using FluentValidation;
using Parking.CrossCutting.Helpers;

namespace Parking.Domain.Entities.Validations
{
    public class CompanyValidation : AbstractValidator<Company>
    {
        public CompanyValidation()
        {
            RuleFor(x => x.Address)
                .NotEmpty().WithMessage("O campo '{PropertyName}' precisa ser fornecido.")
                .NotNull().WithMessage("O campo '{PropertyName}' não pode ser nulo.");

            RuleFor(x => x.Phones)
                .NotEmpty().WithMessage("O campo '{PropertyName}' precisa ser fornecido.")
                .NotNull().WithMessage("O campo '{PropertyName}' não pode ser nulo.");

            RuleFor(x => x.Name)
                .NotEmpty().WithMessage("O campo '{PropertyName}' precisa ser fornecido.")
                .NotNull().WithMessage("O campo '{PropertyName}' não pode ser nulo.")
                .Length(Company.NameMinLength, Company.NameMaxLength).WithMessage("O campo '{PropertyName}' precisa ter entre {MinLength} e {MaxLength} caracteres.");

            RuleFor(x => x.CNPJ.Length).Equal(Company.CNPJMaxLength)
                .WithMessage("O campo 'CNPJ' precisa ter {ComparisonValue} caracteres e foi fornecido {PropertyValue}.");

            RuleFor(x => CnpjValidation.Validate(x.CNPJ)).Equal(true)
                .WithMessage("O 'CNPJ' fornecido é inválido.");
        }
    }
}
