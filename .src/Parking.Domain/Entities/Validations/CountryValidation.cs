﻿using FluentValidation;

namespace Parking.Domain.Entities.Validations
{
    public class CountryValidation : AbstractValidator<Country>
    {
        public CountryValidation()
        {
            RuleFor(x => x.Name)
                .NotEmpty().WithMessage("O campo '{PropertyName}' precisa ser fornecido.")
                .NotNull().WithMessage("O campo '{PropertyName}' não pode ser nulo.")
                .Length(Country.NameMinLength, Country.NameMaxLength).WithMessage("O campo '{PropertyName}' precisa ter entre {MinLength} e {MaxLength} caracteres.");

            RuleFor(x => x.Code)
                .NotEmpty().WithMessage("O campo '{PropertyName}' precisa ser fornecido.")
                .NotNull().WithMessage("O campo '{PropertyName}' não pode ser nulo.")
                .Length(Country.CodeMinLength, Country.CodeMaxLength).WithMessage("O campo '{PropertyName}' precisa ter entre {MinLength} e {MaxLength} caracteres.");
        }
    }
}
