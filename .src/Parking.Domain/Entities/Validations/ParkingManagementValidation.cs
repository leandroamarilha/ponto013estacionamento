﻿using FluentValidation;
using System;

namespace Parking.Domain.Entities.Validations
{
    public class ParkingManagementValidation : AbstractValidator<ParkingManagement>
    {
        public ParkingManagementValidation()
        {
            RuleFor(x => x.DateTimeEntry)
                .NotEmpty().WithMessage("O campo '{PropertyName}' precisa ser fornecido.")
                .NotNull().WithMessage("O campo '{PropertyName}' não pode ser nulo.")
                .Must(date => date == default || date != DateTime.Today).WithMessage("O campo '{PropertyName}' possui uma data inválida.");

            RuleFor(x => x.DateTimeExit)
                .Must(date => date == default || date != DateTime.Today).WithMessage("O campo '{PropertyName}' possui uma data inválida.");

            RuleFor(x => x.VehicleId)
                .NotEmpty().WithMessage("O campo '{PropertyName}' precisa ser fornecido.")
                .NotNull().WithMessage("O campo '{PropertyName}' não pode ser nulo.")
                .GreaterThan(0).WithMessage("O campo '{PropertyName}' precisa ser maior que {ComparisonValue}.");
        }
    }
}
