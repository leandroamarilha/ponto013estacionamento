﻿using FluentValidation;

namespace Parking.Domain.Entities.Validations
{
    public class AddressValidation : AbstractValidator<Address>
    {
        public AddressValidation()
        {
            RuleFor(x => x.Name)
                .NotEmpty().WithMessage("O campo '{PropertyName}' precisa ser fornecido.")
                .NotNull().WithMessage("O campo '{PropertyName}' não pode ser nulo.")
                .Length(Address.NameMinLength, Address.NameMaxLength).WithMessage("O campo '{PropertyName}' precisa ter entre {MinLength} e {MaxLength} caracteres.");

            RuleFor(x => x.Number)
                .NotEmpty().WithMessage("O campo '{PropertyName}' precisa ser fornecido.")
                .NotNull().WithMessage("O campo '{PropertyName}' não pode ser nulo.")
                .MaximumLength(Address.NumberMaxLength).WithMessage("O campo '{PropertyName}' precisa ter no máximo {MaxLength} caracteres.");

            RuleFor(x => x.Complement)
                .MaximumLength(Address.ComplementMaxLength).WithMessage("O campo '{PropertyName}' deve ter no máximo {MaxLength} caracteres.");

            RuleFor(x => x.City)
                .NotEmpty().WithMessage("O campo '{PropertyName}' precisa ser fornecido.")
                .NotNull().WithMessage("O campo '{PropertyName}' não pode ser nulo.")
                .Length(Address.CityMinLength, Address.CityMaxLength).WithMessage("O campo '{PropertyName}' precisa ter entre {MinLength} e {MaxLength} caracteres.");

            RuleFor(x => x.State)
                .NotEmpty().WithMessage("O campo '{PropertyName}' precisa ser fornecido.")
                .NotNull().WithMessage("O campo '{PropertyName}' não pode ser nulo.")
                .Length(Address.StateMinLength, Address.StateMaxLength).WithMessage("O campo '{PropertyName}' precisa ter entre {MinLength} e {MaxLength} caracteres.");

            RuleFor(x => x.ZipCode.Length).Equal(Address.ZipCodeMaxLength)
                .NotEmpty().WithMessage("O campo '{PropertyName}' precisa ser fornecido.")
                .NotNull().WithMessage("O campo '{PropertyName}' não pode ser nulo.")
                .WithMessage("O campo '{PropertyName}' precisa ter {ComparisonValue} caracteres e foi fornecido {PropertyValue}.");

            RuleFor(x => x.CountryId)
                .NotEmpty().WithMessage("O campo '{PropertyName}' precisa ser fornecido.")
                .NotNull().WithMessage("O campo '{PropertyName}' não pode ser nulo.")
                .GreaterThan(0).WithMessage("O campo '{PropertyName}' precisa ser maior que {ComparisonValue}.");
        }
    }
}
