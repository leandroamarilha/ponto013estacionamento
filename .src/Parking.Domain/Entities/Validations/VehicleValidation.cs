﻿using FluentValidation;
using Parking.CrossCutting.Helpers;

namespace Parking.Domain.Entities.Validations
{
    public class VehicleValidation : AbstractValidator<Vehicle>
    {
        public VehicleValidation()
        {
            RuleFor(x => x.Brand)
                .NotEmpty().WithMessage("O campo '{PropertyName}' precisa ser fornecido.")
                .NotNull().WithMessage("O campo '{PropertyName}' não pode ser nulo.")
                .Length(Vehicle.BrandMinLength, Vehicle.BrandMaxLength).WithMessage("O campo '{PropertyName}' precisa ter entre {MinLength} e {MaxLength} caracteres.");

            RuleFor(x => x.Color)
                .NotEmpty().WithMessage("O campo '{PropertyName}' precisa ser fornecido.")
                .NotNull().WithMessage("O campo '{PropertyName}' não pode ser nulo.")
                .Length(Vehicle.ColorMinLength, Vehicle.ColorMaxLength).WithMessage("O campo '{PropertyName}' precisa ter entre {MinLength} e {MaxLength} caracteres.");

            RuleFor(x => x.LicensePlate)
                .NotEmpty().WithMessage("O campo '{PropertyName}' precisa ser fornecido.")
                .NotNull().WithMessage("O campo '{PropertyName}' não pode ser nulo.")
                .Length(Vehicle.LicensePlateMinLength, Vehicle.LicensePlateMaxLength).WithMessage("O campo '{PropertyName}' precisa ter entre {MinLength} e {MaxLength} caracteres.");

            RuleFor(x => x.Model)
                .NotEmpty().WithMessage("O campo '{PropertyName}' precisa ser fornecido.")
                .NotNull().WithMessage("O campo '{PropertyName}' não pode ser nulo.")
                .Length(Vehicle.ModelMinLength, Vehicle.ModelMaxLength).WithMessage("O campo '{PropertyName}' precisa ter entre {MinLength} e {MaxLength} caracteres.");

            RuleFor(x => x.CompanyId)
                .NotEmpty().WithMessage("O campo '{PropertyName}' precisa ser fornecido.")
                .NotNull().WithMessage("O campo '{PropertyName}' não pode ser nulo.")
                .GreaterThan(0).WithMessage("O campo '{PropertyName}' precisa ser maior que {ComparisonValue}.");

            RuleFor(x => x.VehicleTypeId)
                .NotEmpty().WithMessage("O campo '{PropertyName}' precisa ser fornecido.")
                .NotNull().WithMessage("O campo '{PropertyName}' não pode ser nulo.")
                .GreaterThan(0).WithMessage("O campo '{PropertyName}' precisa ser maior que {ComparisonValue}.");

            RuleFor(x => LicensePlateValidation.Validate(x.LicensePlate)).Equal(true)
                .WithMessage("O campo 'LicensePlate' tem um formato de placa inválido.");
        }
    }
}
