﻿using FluentValidation;

namespace Parking.Domain.Entities.Validations
{
    public class ParkingConfigurationValidation : AbstractValidator<ParkingConfiguration>
    {
        public ParkingConfigurationValidation()
        {
            RuleFor(x => x.TotalParking)
                .NotEmpty().WithMessage("O campo '{PropertyName}' precisa ser fornecido.")
                .NotNull().WithMessage("O campo '{PropertyName}' não pode ser nulo.")
                .GreaterThan(0).WithMessage("O campo '{PropertyName}' precisa ser maior que {ComparisonValue}.");
        }
    }
}
