﻿using Parking.Domain.Interfaces.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Parking.Domain.Entities
{
    public class User : EntityBase, IUser
    {
        public const int UserNameMinLength = 5;
        public const int UserNameMaxLength = 100;

        public const int PasswordNameMinLength = 6;
        public const int PasswordNameMaxLength = 200;

        public User()
        {
            DateCreated = DateTime.Now;
        }

        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
