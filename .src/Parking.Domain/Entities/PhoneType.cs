﻿using Parking.Domain.Interfaces.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Parking.Domain.Entities
{
    public class PhoneType : EntityBase, IPhoneType
    {
        public const int TypeMinLength = 2;
        public const int TypeMaxLength = 50;

        public PhoneType()
        {
            DateCreated = DateTime.Now;
        }

        public string Type { get; set; }
    }
}
