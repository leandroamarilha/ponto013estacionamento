﻿using Parking.Domain.Interfaces.Entities;
using System;

namespace Parking.Domain.Entities
{
    public class ParkingManagement : EntityBase, IParkingManagement
    {
        public ParkingManagement()
        {
            DateCreated = DateTime.Now;
        }

        public DateTime DateTimeEntry { get; set; }
        public DateTime? DateTimeExit { get; set; }
        
        public int VehicleId { get; set; }
        public virtual Vehicle Vehicle { get; set; }
    }
}
