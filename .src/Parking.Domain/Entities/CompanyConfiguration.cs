﻿using Parking.Domain.Interfaces.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Parking.Domain.Entities
{
    public class CompanyConfiguration : EntityBase, ICompanyConfiguration
    {
        public CompanyConfiguration()
        {
            DateCreated = DateTime.Now;
        }

        public int ParkingGarageTotal { get; set; }

        public int VehicleTypeId { get; set; }
        public VehicleType VehicleType { get; set; }

        public int CompanyId { get; set; }
        public Company Company { get; set; }
    }
}
