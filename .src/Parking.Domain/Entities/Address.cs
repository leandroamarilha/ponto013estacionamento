﻿using Parking.Domain.Interfaces.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Parking.Domain.Entities
{
    public class Address : EntityBase, IAddress
    {
        public const int NameMinLength = 2;
        public const int NameMaxLength = 200;

        public const int NumberMinLength = 1;
        public const int NumberMaxLength = 30;

        public const int ComplementMaxLength = 150;

        public const int CityMinLength = 2;
        public const int CityMaxLength = 100;

        public const int StateMinLength = 2;
        public const int StateMaxLength = 100;

        public const int ZipCodeMinLength = 8;
        public const int ZipCodeMaxLength = 8;

        public Address()
        {
            DateCreated = DateTime.Now;
        }

        public string Name { get; set; }
        public string Number { get; set; }
        public string Complement { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }

        public int CompanyId { get; set; }
        public Company Company { get; set; }

        public virtual Country Country { get; set; }
        public int CountryId { get; set; }
    }
}
