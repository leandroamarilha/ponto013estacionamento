﻿using Parking.Domain.Interfaces.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Parking.Domain.Entities
{
    public class Vehicle : EntityBase, IVehicle
    {
        public const int BrandMinLength = 2;
        public const int BrandMaxLength = 100;

        public const int ModelMinLength = 2;
        public const int ModelMaxLength = 100;

        public const int ColorMinLength = 2;
        public const int ColorMaxLength = 50;

        public const int LicensePlateMinLength = 7;
        public const int LicensePlateMaxLength = 7;

        public Vehicle()
        {
            DateCreated = DateTime.Now;
        }

        public string Brand { get; set; }
        public string Model { get; set; }
        public string Color { get; set; }
        public string LicensePlate { get; set; }

        public int VehicleTypeId { get; set; }
        public virtual VehicleType VehicleType { get; set; }

        public int CompanyId { get; set; }
        public virtual Company Company { get; set; }
    }
}
