﻿using Parking.Domain.Interfaces.Entities;
using System;

namespace Parking.Domain.Entities
{
    public class ParkingConfiguration : EntityBase, IParkingConfiguration
    {
        public ParkingConfiguration()
        {
            DateCreated = DateTime.Now;
        }

        public int TotalParking { get; set; }
    }
}
