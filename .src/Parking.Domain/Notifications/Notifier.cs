﻿using Parking.Domain.Interfaces.Notifications;
using System.Collections.Generic;
using System.Linq;

namespace Parking.Domain.Notifications
{
    public class Notifier : INotifier
    {
        protected List<Notification> _notifications;

        public Notifier()
        {
            _notifications = new List<Notification>();
        }

        public void Handle(Notification notification)
        {
            _notifications.Add(notification);
        }

        public List<Notification> GetNotifications()
        {
            return _notifications;
        }

        public bool HasNotification()
        {
            return _notifications.Any();
        }
    }
}
