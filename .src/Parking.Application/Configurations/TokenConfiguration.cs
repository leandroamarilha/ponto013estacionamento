﻿using Parking.Application.Interfaces.Configurations;
using System;

namespace Parking.Application.Configurations
{
    public class TokenConfiguration : ITokenConfiguration
    {
        public string Key { get; set; }
        public string Audience { get; set; }
        public string Issuer { get; set; }
        public uint ExpireSeconds { get; set; }
        public TimeSpan Expire { get { return TimeSpan.FromSeconds(ExpireSeconds); } }
    }
}
