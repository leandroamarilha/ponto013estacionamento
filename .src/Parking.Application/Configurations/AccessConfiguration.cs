﻿using Parking.Application.Interfaces.Configurations;

namespace Parking.Application.Configurations
{
    public class AccessConfiguration : IAccessConfiguration
    {
        public string FileEmailTemplate { get; set; }
        public double VerificationCodeExpiresMinutes { get; set; }
        public string CodeConfirmationKey { get; set; }
        public string EmailLinkConfirmation { get; set; }
        public string EmailFrom { get; set; }
    }
}
