﻿using Parking.Application.Interfaces.Configurations;
using Parking.Application.Interfaces.Security;
using Parking.Application.ViewsModels.Login;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Parking.CrossCutting.Security.Encryption;

namespace Parking.Application.Security
{
    public class TokenSecurity : ITokenSecurity
    {
        private readonly ITokenConfiguration _tokenConfiguration;

        public TokenSecurity(ITokenConfiguration tokenConfiguration)
        {
            _tokenConfiguration = tokenConfiguration;
        }

        public TokenViewModel Get(IList<Claim> claims)
        {
            var token = new JwtSecurityToken(
                    issuer: _tokenConfiguration.Issuer,
                    audience: _tokenConfiguration.Audience,
                    expires: DateTime.Now.Add(_tokenConfiguration.Expire),
                    signingCredentials: _tokenConfiguration.Key.GetSigningCredentials(),
                    claims: claims,
                    notBefore: DateTime.Now
                );

            return new TokenViewModel()
            {
                AccessToken = new JwtSecurityTokenHandler().WriteToken(token),
                TokenID = token.Id,
                Expires = token.ValidTo,
                Create = token.ValidFrom
            };
        }
    }
}
