﻿using AutoMapper.Configuration.Annotations;
using System;
using System.ComponentModel.DataAnnotations;

namespace Parking.Application.ViewsModels
{
    public class PhoneTypeViewModel
    {
        public PhoneTypeViewModel()
        {
            DateCreated = DateTime.Now;
        }

        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "O campo '{0}' é obrigatório")]
        [StringLength(50, ErrorMessage = "O campo {0} precisa ter entre {2} e {1} caracteres", MinimumLength = 2)]
        public string Type { get; set; }

        [Ignore]
        public DateTime DateCreated { get; set; }
    }
}
