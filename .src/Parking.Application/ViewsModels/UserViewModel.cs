﻿using AutoMapper.Configuration.Annotations;
using System;
using System.ComponentModel.DataAnnotations;

namespace Parking.Application.ViewsModels
{
    public class UserViewModel
    {
        public UserViewModel()
        {
            DateCreated = DateTime.Now;
        }

        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "O campo '{0}' é obrigatório")]
        [StringLength(100, ErrorMessage = "O campo '{0}' precisa ter entre {2} e {1} caracteres", MinimumLength = 8)]
        public string UserName { get; set; }

        [Required(ErrorMessage = "O campo '{0}' é obrigatório")]
        [StringLength(200, ErrorMessage = "O campo '{0}' precisa ter entre {2} e {1} caracteres", MinimumLength = 12)]
        public string Password { get; set; }

        [Ignore]
        public DateTime DateCreated { get; set; }
    }
}
