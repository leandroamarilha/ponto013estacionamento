﻿using AutoMapper.Configuration.Annotations;
using System;
using System.ComponentModel.DataAnnotations;

namespace Parking.Application.ViewsModels
{
    public class ParkingConfigurationViewModel
    {
        public ParkingConfigurationViewModel()
        {
            DateCreated = DateTime.Now;
        }

        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "O campo '{0}' é obrigatório")]
        public int TotalParking { get; set; }

        [Ignore]
        public DateTime DateCreated { get; set; }
    }
}
