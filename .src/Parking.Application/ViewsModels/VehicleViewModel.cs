﻿using AutoMapper.Configuration.Annotations;
using System;
using System.ComponentModel.DataAnnotations;

namespace Parking.Application.ViewsModels
{
    public class VehicleViewModel
    {
        public VehicleViewModel()
        {
            DateCreated = DateTime.Now;
        }

        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "O campo '{0}' é obrigatório")]
        [StringLength(100, ErrorMessage = "O campo '{0}' precisa ter entre {2} e {1} caracteres", MinimumLength = 2)]
        public string Brand { get; set; }

        [Required(ErrorMessage = "O campo '{0}' é obrigatório")]
        [StringLength(100, ErrorMessage = "O campo '{0}' precisa ter entre {2} e {1} caracteres", MinimumLength = 2)]
        public string Model { get; set; }

        [Required(ErrorMessage = "O campo '{0}' é obrigatório")]
        [StringLength(50, ErrorMessage = "O campo '{0}' precisa ter entre {2} e {1} caracteres", MinimumLength = 2)]
        public string Color { get; set; }

        [Required(ErrorMessage = "O campo '{0}' é obrigatório")]
        [StringLength(7, ErrorMessage = "O campo '{0}' precisa ter entre {2} e {1} caracteres", MinimumLength = 7)]
        public string LicensePlate { get; set; }

        [Required(ErrorMessage = "O campo '{0}' é obrigatório")]
        public int VehicleTypeId { get; set; }

        public VehicleTypeViewModel VehicleType { get; set; }

        [Required(ErrorMessage = "O campo '{0}' é obrigatório")]
        public int CompanyId { get; set; }
        public CompanyViewModel Company { get; set; }

        [Ignore]
        public DateTime DateCreated { get; set; }
    }
}
