﻿using AutoMapper.Configuration.Annotations;
using System;
using System.ComponentModel.DataAnnotations;

namespace Parking.Application.ViewsModels
{
    public class ParkingManagementViewModel
    {
        public ParkingManagementViewModel()
        {
            DateCreated = DateTime.Now;
        }

        [Key]
        public int Id { get; set; }

        public DateTime DateTimeEntry { get; set; }
        public DateTime? DateTimeExit { get; set; }

        //[Required(ErrorMessage = "O campo '{0}' é obrigatório")]
        public int VehicleId { get; set; }
        public virtual VehicleViewModel Vehicle { get; set; }

        [Required(ErrorMessage = "O campo '{0}' é obrigatório")]
        [StringLength(7, ErrorMessage = "O campo '{0}' precisa ter entre {2} e {1} caracteres", MinimumLength = 7)]
        public string LicensePlate { get; set; }

        [Ignore]
        public DateTime DateCreated { get; set; }
    }
}
