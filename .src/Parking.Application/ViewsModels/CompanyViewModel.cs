﻿using AutoMapper.Configuration.Annotations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Parking.Application.ViewsModels
{
    public class CompanyViewModel
    {
        public CompanyViewModel()
        {
            DateCreated = DateTime.Now;
        }

        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "O campo '{0}' é obrigatório")]
        [StringLength(200, ErrorMessage = "O campo '{0}' precisa ter entre {2} e {1} caracteres", MinimumLength = 2)]
        public string Name { get; set; }

        [Required(ErrorMessage = "O campo '{0}' é obrigatório")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "O campo '{0}' deve conter apenas números.")]
        [StringLength(14, ErrorMessage = "O campo '{0}' precisa ter entre {2} e {1} caracteres", MinimumLength = 14)]
        public string CNPJ { get; set; }

        [Required(ErrorMessage = "O campo '{0}' é obrigatório")]
        public int AddressId { get; set; }

        [Required(ErrorMessage = "O campo '{0}' é obrigatório")]
        public AddressViewModel Address { get; set; }

        [Required(ErrorMessage = "O campo '{0}' é obrigatório")]
        public ICollection<PhoneViewModel> Phones { get; set; }

        [Required(ErrorMessage = "O campo '{0}' é obrigatório")]
        public ICollection<CompanyConfigurationViewModel> CompanyConfigurations { get; set; }

        [Ignore]
        public DateTime DateCreated { get; set; }
    }
}
