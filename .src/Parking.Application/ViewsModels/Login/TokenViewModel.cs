﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Parking.Application.ViewsModels.Login
{
    public class TokenViewModel
    {
        [Required]
        public string TokenID { get; set; }
        [Required]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        [Required]
        public DateTime Create { get; set; }
        [Required]
        public DateTime Expires { get; set; }
    }
}
