﻿using Parking.Application.Interfaces.ViewsModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Parking.Application.ViewsModels.Login
{
    public class UserLoginViewModel : IUserLoginViewModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
