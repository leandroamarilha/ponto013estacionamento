﻿using AutoMapper.Configuration.Annotations;
using System;
using System.ComponentModel.DataAnnotations;

namespace Parking.Application.ViewsModels
{
    public class CompanyConfigurationViewModel
    {
        public CompanyConfigurationViewModel()
        {
            DateCreated = DateTime.Now;
        }

        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "O campo '{0}' é obrigatório")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "{0} é um campo numérico.")]
        public int ParkingGarageTotal { get; set; }

        [Required(ErrorMessage = "O campo '{0}' é obrigatório")]
        public int VehicleTypeId { get; set; }

        public VehicleTypeViewModel VehicleType { get; set; }

        public int CompanyId { get; set; }

        public CompanyViewModel Company { get; set; }

        [Ignore]
        public DateTime DateCreated { get; set; }
    }
}
