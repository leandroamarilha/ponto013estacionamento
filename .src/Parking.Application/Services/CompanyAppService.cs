﻿using AutoMapper;
using Parking.Application.Interfaces;
using Parking.Application.ViewsModels;
using Parking.Domain.Entities;
using Parking.Domain.Interfaces.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Parking.Application.Services
{
    public class CompanyAppService : ICompanyAppService
    {
        private readonly IMapper _mapper;
        private readonly ICompanyService _companyService;

        public CompanyAppService(ICompanyService companyService,
                                 IMapper mapper)
        {
            _companyService = companyService;
            _mapper = mapper;
        }

        public async Task Insert(CompanyViewModel companyViewModel)
        {
            await _companyService.Insert(_mapper.Map<Company>(companyViewModel));
        }

        public async Task<CompanyViewModel> InsertWithReturn(CompanyViewModel companyViewModel)
        {
            var company = await _companyService.InsertWithReturn(_mapper.Map<Company>(companyViewModel));

            return _mapper.Map<CompanyViewModel>(company);
        }

        public async Task Update(int id, CompanyViewModel companyViewModel)
        {
            await _companyService.Update(id, _mapper.Map<Company>(companyViewModel));
        }

        public async Task<CompanyViewModel> UpdateWithReturn(int id, CompanyViewModel companyViewModel)
        {
            var company = await _companyService.UpdateWithReturn(id, _mapper.Map<Company>(companyViewModel));

            return _mapper.Map<CompanyViewModel>(company);
        }

        public async Task<List<CompanyViewModel>> GetAll()
        {
            var company = await _companyService.GetAll();

            if (company == null || company.Count == 0) return null;

            return _mapper.Map<List<CompanyViewModel>>(company);
        }

        public async Task<CompanyViewModel> GetbyId(int id)
        {
            var company = await _companyService.GetById(id);

            if (company == null) return null;

            return _mapper.Map<CompanyViewModel>(company);
        }

        public async Task<CompanyViewModel> GetFullById(int id)
        {
            var company = await _companyService.GetFullById(id);

            if (company == null) return null;

            return _mapper.Map<CompanyViewModel>(company);
        }

        public async Task Delete(int id)
        {
            await _companyService.Delete(id);
        }

        public async Task<bool> Exist(int id)
        {
            return await _companyService.Exist(id);
        }
    }
}
