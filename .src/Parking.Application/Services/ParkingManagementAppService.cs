﻿using AutoMapper;
using Parking.Application.Interfaces;
using Parking.Application.ViewsModels;
using Parking.Domain.Entities;
using Parking.Domain.Interfaces.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Parking.Application.Services
{
    public class ParkingManagementAppService : IParkingManagementAppService
    {
        private readonly IMapper _mapper;
        private readonly IParkingManagementService _parkingManagementService;

        public ParkingManagementAppService(IParkingManagementService parkingManagementService,
                                           IMapper mapper)
        {
            _parkingManagementService = parkingManagementService;
            _mapper = mapper;
        }

        public async Task Delete(int id)
        {
            await _parkingManagementService.Delete(id);
        }

        public async Task<bool> Exist(int id)
        {
            return await _parkingManagementService.Exist(id);
        }

        public async Task<List<ParkingManagementViewModel>> GetAll()
        {
            var parkingManagements = await _parkingManagementService.GetAll();

            if (parkingManagements == null || parkingManagements.Count == 0) return null;

            return _mapper.Map<List<ParkingManagementViewModel>>(parkingManagements);
        }

        public async Task<ParkingManagementViewModel> GetbyId(int id)
        {
            var parkingManagement = await _parkingManagementService.GetById(id);

            if (parkingManagement == null) return null;

            return _mapper.Map<ParkingManagementViewModel>(parkingManagement);
        }

        public async Task<ParkingManagementViewModel> GetFullById(int id)
        {
            var parkingManagement = await _parkingManagementService.GetFullById(id);

            if (parkingManagement == null) return null;

            return _mapper.Map<ParkingManagementViewModel>(parkingManagement);
        }

        public async Task<ParkingManagementViewModel> GetFullByLicensePlate(string licensePlate)
        {
            var parkingManagement = await _parkingManagementService.GetFullByLicensePlate(licensePlate);

            if (parkingManagement == null) return null;

            return _mapper.Map<ParkingManagementViewModel>(parkingManagement);
        }

        public async Task Insert(ParkingManagementViewModel parkingManagementViewModel)
        {
            await _parkingManagementService.Insert(_mapper.Map<ParkingManagement>(parkingManagementViewModel), parkingManagementViewModel.LicensePlate);
        }

        public async Task<ParkingManagementViewModel> InsertWithReturn(ParkingManagementViewModel parkingManagementViewModel)
        {
            var parkingManagement = await _parkingManagementService.InsertWithReturn(_mapper.Map<ParkingManagement>(parkingManagementViewModel), parkingManagementViewModel.LicensePlate);

            return _mapper.Map<ParkingManagementViewModel>(parkingManagement);
        }

        public async Task Update(ParkingManagementViewModel parkingManagementViewModel)
        {
            await _parkingManagementService.Update(_mapper.Map<ParkingManagement>(parkingManagementViewModel), parkingManagementViewModel.LicensePlate);
        }

        public async Task<ParkingManagementViewModel> UpdateWithReturn(ParkingManagementViewModel parkingManagementViewModel)
        {
            var parkingManagement = await _parkingManagementService.UpdateWithReturn(_mapper.Map<ParkingManagement>(parkingManagementViewModel), parkingManagementViewModel.LicensePlate);

            return _mapper.Map<ParkingManagementViewModel>(parkingManagement);
        }
    }
}
