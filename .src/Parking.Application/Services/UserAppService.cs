﻿using System;
using AutoMapper;
using Parking.Application.Interfaces;
using Parking.Application.ViewsModels;
using Parking.Application.ViewsModels.Login;
using Parking.Domain.Entities;
using Parking.Domain.Interfaces.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Security.Claims;
using Parking.Application.Interfaces.Security;
using Parking.Application.Interfaces.ViewsModels;

namespace Parking.Application.Services
{
    public class UserAppService : IUserAppService
    {
        private readonly IMapper _mapper;
        private readonly IUserService _userService;
        private readonly ITokenSecurity _tokenSecurity;

        public UserAppService(IUserService userService,
                              ITokenSecurity tokenSecurity,
                              IMapper mapper)
        {
            _userService = userService;
            _tokenSecurity = tokenSecurity;
            _mapper = mapper;
        }

        #region Login

        public async Task<TokenViewModel> Login(IUserLoginViewModel loginViewModel)
        {
            var user = await _userService.VerifyLogin(_mapper.Map<User>(loginViewModel));

            if (user == null) return null;

            return CreateClaims(user);
        }

        private TokenViewModel CreateClaims(User user)
        {
            IList<Claim> claims = new List<Claim>
            {
                new Claim("Id", user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.UserName),
                new Claim(ClaimTypes.NameIdentifier, user.UserName)
            };

            return _tokenSecurity.Get(claims);
        }

        #endregion

        public async Task Delete(int id)
        {
            await _userService.Delete(id);
        }

        public async Task<bool> Exist(int id)
        {
            return await _userService.Exist(id);
        }

        public async Task<List<UserViewModel>> GetAll()
        {
            var users = await _userService.GetAll();

            if (users == null || users.Count == 0) return null;

            return _mapper.Map<List<UserViewModel>>(users);
        }

        public async Task<UserViewModel> GetbyId(int id)
        {
            var user = await _userService.GetById(id);

            if (user == null) return null;

            return _mapper.Map<UserViewModel>(user);
        }

        public async Task<UserViewModel> GetFullById(int id)
        {
            var user = await _userService.GetFullById(id);

            if (user == null) return null;

            return _mapper.Map<UserViewModel>(user);
        }

        public async Task Insert(UserViewModel userViewModel)
        {
            await _userService.Insert(_mapper.Map<User>(userViewModel));
        }

        public async Task<UserViewModel> InsertWithReturn(UserViewModel userViewModel)
        {
            var user = await _userService.InsertWithReturn(_mapper.Map<User>(userViewModel));

            return _mapper.Map<UserViewModel>(user);
        }

        public async Task Update(int id, UserViewModel userViewModel)
        {
            await _userService.Update(id, _mapper.Map<User>(userViewModel));
        }

        public async Task<UserViewModel> UpdateWithReturn(int id, UserViewModel userViewModel)
        {
            var user = await _userService.UpdateWithReturn(id, _mapper.Map<User>(userViewModel));

            return _mapper.Map<UserViewModel>(user);
        }
    }
}
