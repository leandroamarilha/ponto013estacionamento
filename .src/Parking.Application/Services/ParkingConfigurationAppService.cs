﻿using AutoMapper;
using Parking.Application.Interfaces;
using Parking.Application.ViewsModels;
using Parking.Domain.Entities;
using Parking.Domain.Interfaces.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Parking.Application.Services
{
    public class ParkingConfigurationAppService : IParkingConfigurationAppService
    {
        private readonly IMapper _mapper;
        private readonly IParkingConfigurationService _parkingConfigurationService;

        public ParkingConfigurationAppService(IParkingConfigurationService parkingConfigurationService,
                                              IMapper mapper)
        {
            _parkingConfigurationService = parkingConfigurationService;
            _mapper = mapper;
        }

        public async Task Delete(int id)
        {
            await _parkingConfigurationService.Delete(id);
        }

        public async Task<bool> Exist(int id)
        {
            return await _parkingConfigurationService.Exist(id);
        }

        public async Task<List<ParkingConfigurationViewModel>> GetAll()
        {
            var parkingConfigurations = await _parkingConfigurationService.GetAll();

            if (parkingConfigurations == null || parkingConfigurations.Count == 0) return null;

            return _mapper.Map<List<ParkingConfigurationViewModel>>(parkingConfigurations);
        }

        public async Task<ParkingConfigurationViewModel> GetbyId(int id)
        {
            var parkingConfiguration = await _parkingConfigurationService.GetById(id);

            if (parkingConfiguration == null) return null;

            return _mapper.Map<ParkingConfigurationViewModel>(parkingConfiguration);
        }

        public async Task<ParkingConfigurationViewModel> GetFullById(int id)
        {
            var companyConfiguration = await _parkingConfigurationService.GetFullById(id);

            if (companyConfiguration == null) return null;

            return _mapper.Map<ParkingConfigurationViewModel>(companyConfiguration);
        }

        public async Task Insert(ParkingConfigurationViewModel parkingConfigurationViewModel)
        {
            await _parkingConfigurationService.Insert(_mapper.Map<ParkingConfiguration>(parkingConfigurationViewModel));
        }

        public async Task<ParkingConfigurationViewModel> InsertWithReturn(ParkingConfigurationViewModel parkingConfigurationViewModel)
        {
            var parkingConfiguration = await _parkingConfigurationService.InsertWithReturn(_mapper.Map<ParkingConfiguration>(parkingConfigurationViewModel));

            return _mapper.Map<ParkingConfigurationViewModel>(parkingConfiguration);
        }

        public async Task Update(int id, ParkingConfigurationViewModel parkingConfigurationViewModel)
        {
            await _parkingConfigurationService.Update(id, _mapper.Map<ParkingConfiguration>(parkingConfigurationViewModel));
        }

        public async Task<ParkingConfigurationViewModel> UpdateWithReturn(int id, ParkingConfigurationViewModel parkingConfigurationViewModel)
        {
            var parkingConfiguration = await _parkingConfigurationService.UpdateWithReturn(id, _mapper.Map<ParkingConfiguration>(parkingConfigurationViewModel));

            return _mapper.Map<ParkingConfigurationViewModel>(parkingConfiguration);
        }
    }
}
