﻿using AutoMapper;
using Parking.Application.Interfaces;
using Parking.Application.ViewsModels;
using Parking.Domain.Entities;
using Parking.Domain.Interfaces.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Parking.Application.Services
{
    public class VehicleAppService : IVehicleAppService
    {
        private readonly IMapper _mapper;
        private readonly IVehicleService _vehicleService;

        public VehicleAppService(IVehicleService vehicleService,
                                 IMapper mapper)
        {
            _vehicleService = vehicleService;
            _mapper = mapper;
        }

        public async Task Delete(int id)
        {
            await _vehicleService.Delete(id);
        }

        public async Task<bool> Exist(int id)
        {
            return await _vehicleService.Exist(id);
        }

        public async Task<bool> ExistByLicensePlate(string licensePlate)
        {
            return await _vehicleService.ExistByLicensePlate(licensePlate);
        }

        public async Task<List<VehicleViewModel>> GetAll()
        {
            var vehicles = await _vehicleService.GetAll();

            if (vehicles == null || vehicles.Count == 0) return null;

            return _mapper.Map<List<VehicleViewModel>>(vehicles);
        }

        public async Task<VehicleViewModel> GetbyId(int id)
        {
            var vehicle = await _vehicleService.GetById(id);

            if (vehicle == null) return null;

            return _mapper.Map<VehicleViewModel>(vehicle);
        }

        public async Task<VehicleViewModel> GetFullById(int id)
        {
            var vehicle = await _vehicleService.GetFullById(id);

            if (vehicle == null) return null;

            return _mapper.Map<VehicleViewModel>(vehicle);
        }

        public async Task<VehicleViewModel> GetVehicleByLicensePlate(string licensePlate)
        {
            var vehicle = await _vehicleService.GetVehicleByLicensePlate(licensePlate);

            if (vehicle == null) return null;

            return _mapper.Map<VehicleViewModel>(vehicle);
        }

        public async Task Insert(VehicleViewModel vehicleViewModel)
        {
            await _vehicleService.Insert(_mapper.Map<Vehicle>(vehicleViewModel));
        }

        public async Task<VehicleViewModel> InsertWithReturn(VehicleViewModel vehicleViewModel)
        {
            var vehicle = await _vehicleService.InsertWithReturn(_mapper.Map<Vehicle>(vehicleViewModel));

            return _mapper.Map<VehicleViewModel>(vehicle);
        }

        public async Task Update(int id, VehicleViewModel vehicleViewModel)
        {
            await _vehicleService.Update(id, _mapper.Map<Vehicle>(vehicleViewModel));
        }

        public async Task<VehicleViewModel> UpdateWithReturn(int id, VehicleViewModel vehicleViewModel)
        {
            var vehicle = await _vehicleService.UpdateWithReturn(id, _mapper.Map<Vehicle>(vehicleViewModel));

            return _mapper.Map<VehicleViewModel>(vehicle);
        }
    }
}
