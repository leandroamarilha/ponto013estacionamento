﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Parking.Application.Interfaces.ViewsModels
{
    public interface IUserLoginViewModel
    {
        string UserName { get; set; }
        string Password { get; set; }
    }
}
