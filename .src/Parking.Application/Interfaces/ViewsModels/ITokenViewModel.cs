﻿using Newtonsoft.Json;
using System;

namespace Parking.Application.Interfaces.ViewsModels
{
    public interface ITokenViewModel
    {
        string TokenID { get; set; }
        [JsonProperty("access_token")]
        string AccessToken { get; set; }
        DateTime Create { get; set; }
        DateTime Expires { get; set; }
    }
}
