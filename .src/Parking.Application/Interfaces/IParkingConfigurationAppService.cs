﻿using Parking.Application.ViewsModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Parking.Application.Interfaces
{
    public interface IParkingConfigurationAppService
    {
        Task Insert(ParkingConfigurationViewModel parkingConfigurationViewModel);
        Task<ParkingConfigurationViewModel> InsertWithReturn(ParkingConfigurationViewModel parkingConfigurationViewModel);
        Task Update(int id, ParkingConfigurationViewModel parkingConfigurationViewModel);
        Task<ParkingConfigurationViewModel> UpdateWithReturn(int id, ParkingConfigurationViewModel parkingConfigurationViewModel);
        Task Delete(int id);
        Task<bool> Exist(int id);
        Task<ParkingConfigurationViewModel> GetbyId(int id);
        Task<ParkingConfigurationViewModel> GetFullById(int id);
        Task<List<ParkingConfigurationViewModel>> GetAll();
    }
}
