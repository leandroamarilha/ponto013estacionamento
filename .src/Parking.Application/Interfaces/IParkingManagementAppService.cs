﻿using Parking.Application.ViewsModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Parking.Application.Interfaces
{
    public interface IParkingManagementAppService
    {
        Task Insert(ParkingManagementViewModel parkingManagementViewModel);
        Task<ParkingManagementViewModel> InsertWithReturn(ParkingManagementViewModel parkingManagementViewModel);
        Task Update(ParkingManagementViewModel parkingManagementViewModel);
        Task<ParkingManagementViewModel> UpdateWithReturn(ParkingManagementViewModel parkingManagementViewModel);
        Task Delete(int id);
        Task<bool> Exist(int id);
        Task<ParkingManagementViewModel> GetbyId(int id);
        Task<ParkingManagementViewModel> GetFullById(int id);
        Task<ParkingManagementViewModel> GetFullByLicensePlate(string licensePlate);
        Task<List<ParkingManagementViewModel>> GetAll();
    }
}
