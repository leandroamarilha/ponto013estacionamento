﻿using Parking.Application.ViewsModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Parking.Application.Interfaces
{
    public interface ICompanyAppService
    {
        Task Insert(CompanyViewModel companyViewModel);
        Task<CompanyViewModel> InsertWithReturn(CompanyViewModel companyViewModel);
        Task Update(int id, CompanyViewModel companyViewModel);
        Task<CompanyViewModel> UpdateWithReturn(int id, CompanyViewModel companyViewModel);
        Task Delete(int id);
        Task<bool> Exist(int id);
        Task<CompanyViewModel> GetbyId(int id);
        Task<CompanyViewModel> GetFullById(int id);
        Task<List<CompanyViewModel>> GetAll();
    }
}
