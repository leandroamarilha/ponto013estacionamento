﻿using Parking.Application.Interfaces.ViewsModels;
using Parking.Application.ViewsModels;
using Parking.Application.ViewsModels.Login;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Parking.Application.Interfaces
{
    public interface IUserAppService
    {
        Task<TokenViewModel> Login(IUserLoginViewModel loginViewModel);
        Task Insert(UserViewModel userViewModel);
        Task<UserViewModel> InsertWithReturn(UserViewModel userViewModel);
        Task Update(int id, UserViewModel userViewModel);
        Task<UserViewModel> UpdateWithReturn(int id, UserViewModel userViewModel);
        Task Delete(int id);
        Task<bool> Exist(int id);
        Task<UserViewModel> GetbyId(int id);
        Task<UserViewModel> GetFullById(int id);
        Task<List<UserViewModel>> GetAll();
    }
}
