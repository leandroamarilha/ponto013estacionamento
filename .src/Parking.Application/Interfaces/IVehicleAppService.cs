﻿using Parking.Application.ViewsModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Parking.Application.Interfaces
{
    public interface IVehicleAppService
    {
        Task Insert(VehicleViewModel vehicleViewModel);
        Task<VehicleViewModel> InsertWithReturn(VehicleViewModel vehicleViewModel);
        Task Update(int id, VehicleViewModel vehicleViewModel);
        Task<VehicleViewModel> UpdateWithReturn(int id, VehicleViewModel vehicleViewModel);
        Task Delete(int id);
        Task<bool> Exist(int id);
        Task<bool> ExistByLicensePlate(string licensePlate);
        Task<VehicleViewModel> GetbyId(int id);
        Task<VehicleViewModel> GetVehicleByLicensePlate(string licensePlate);
        Task<VehicleViewModel> GetFullById(int id);
        Task<List<VehicleViewModel>> GetAll();
    }
}
