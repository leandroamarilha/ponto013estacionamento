﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Parking.Application.Interfaces.Configurations
{
    public interface ITokenConfiguration
    {
        string Key { get; set; }
        string Audience { get; set; }
        string Issuer { get; set; }
        uint ExpireSeconds { get; set; }
        TimeSpan Expire { get { return TimeSpan.FromSeconds(ExpireSeconds); } }
    }
}
