﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Parking.Application.Interfaces.Configurations
{
    public interface IAccessConfiguration
    {
        string FileEmailTemplate { get; set; }
        double VerificationCodeExpiresMinutes { get; set; }
        string CodeConfirmationKey { get; set; }
        string EmailLinkConfirmation { get; set; }
        string EmailFrom { get; set; }
    }
}
