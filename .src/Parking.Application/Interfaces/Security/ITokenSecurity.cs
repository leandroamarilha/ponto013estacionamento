﻿using Parking.Application.ViewsModels.Login;
using System.Collections.Generic;
using System.Security.Claims;

namespace Parking.Application.Interfaces.Security
{
    public interface ITokenSecurity
    {
        TokenViewModel Get(IList<Claim> claims);
    }
}
