﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Parking.Data.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Company",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "varchar(200)", nullable: false),
                    CNPJ = table.Column<string>(type: "varchar(14)", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Company", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Country",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "varchar(200)", nullable: false),
                    Code = table.Column<string>(type: "varchar(2)", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Country", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ParkingConfiguration",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TotalParking = table.Column<int>(type: "int", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ParkingConfiguration", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PhoneType",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Type = table.Column<string>(type: "varchar(50)", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PhoneType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserName = table.Column<string>(type: "varchar(100)", nullable: false),
                    Password = table.Column<string>(type: "varchar(200)", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "VehicleType",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Type = table.Column<string>(type: "varchar(50)", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Address",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "varchar(200)", nullable: false),
                    Number = table.Column<string>(type: "varchar(30)", nullable: false),
                    Complement = table.Column<string>(type: "varchar(150)", nullable: false),
                    City = table.Column<string>(type: "varchar(100)", nullable: false),
                    State = table.Column<string>(type: "varchar(100)", nullable: false),
                    ZipCode = table.Column<string>(type: "varchar(8)", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false),
                    CountryId = table.Column<int>(type: "int", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Address", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Address_Company_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Address_Country_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Country",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Phone",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Number = table.Column<string>(type: "varchar(14)", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false),
                    PhoneTypeId = table.Column<int>(type: "int", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Phone", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Phone_Company_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Phone_PhoneType_PhoneTypeId",
                        column: x => x.PhoneTypeId,
                        principalTable: "PhoneType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CompanyConfiguration",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ParkingGarageTotal = table.Column<int>(type: "int", nullable: false),
                    VehicleTypeId = table.Column<int>(type: "int", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompanyConfiguration", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CompanyConfiguration_Company_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CompanyConfiguration_VehicleType_VehicleTypeId",
                        column: x => x.VehicleTypeId,
                        principalTable: "VehicleType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Vehicle",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Brand = table.Column<string>(type: "varchar(100)", nullable: false),
                    Model = table.Column<string>(type: "varchar(100)", nullable: false),
                    Color = table.Column<string>(type: "varchar(50)", nullable: false),
                    LicensePlate = table.Column<string>(type: "varchar(7)", nullable: false),
                    VehicleTypeId = table.Column<int>(type: "int", nullable: false),
                    CompanyId = table.Column<int>(type: "int", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vehicle", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Vehicle_Company_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Vehicle_VehicleType_VehicleTypeId",
                        column: x => x.VehicleTypeId,
                        principalTable: "VehicleType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ParkingManagement",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DateTimeEntry = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DateTimeExit = table.Column<DateTime>(type: "datetime2", nullable: true),
                    VehicleId = table.Column<int>(type: "int", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ParkingManagement", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ParkingManagement_Vehicle_VehicleId",
                        column: x => x.VehicleId,
                        principalTable: "Vehicle",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Country",
                columns: new[] { "Id", "Code", "DateCreated", "Name" },
                values: new object[,]
                {
                    { 1, "AF", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(6988), "Afghanistan" },
                    { 157, "NZ", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8410), "New Zealand" },
                    { 158, "NI", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8412), "Nicaragua" },
                    { 159, "NE", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8414), "Niger" },
                    { 160, "NG", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8416), "Nigeria" },
                    { 161, "NU", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8418), "Niue" },
                    { 162, "NF", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8420), "Norfolk Island" },
                    { 163, "MP", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8422), "Northern Mariana Islands" },
                    { 164, "NO", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8425), "Norway" },
                    { 165, "OM", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8427), "Oman" },
                    { 166, "PK", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8429), "Pakistan" },
                    { 167, "PW", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8431), "Palau" },
                    { 168, "PS", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8433), "Palestinian Territory, Occupied" },
                    { 156, "NC", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8408), "New Caledonia" },
                    { 169, "PA", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8435), "Panama" },
                    { 171, "PY", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8440), "Paraguay" },
                    { 172, "PE", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8442), "Peru" },
                    { 173, "PH", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8444), "Philippines" },
                    { 174, "PN", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8446), "Pitcairn" },
                    { 175, "PL", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8448), "Poland" },
                    { 176, "PT", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8450), "Portugal" },
                    { 177, "PR", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8452), "Puerto Rico" },
                    { 178, "QA", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8454), "Qatar" },
                    { 179, "RE", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8456), "Reunion" },
                    { 180, "RO", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8458), "Romania" },
                    { 181, "RU", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8460), "Russian Federation" },
                    { 182, "RW", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8462), "RWANDA" },
                    { 170, "PG", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8438), "Papua New Guinea" },
                    { 155, "AN", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8406), "Netherlands Antilles" },
                    { 154, "NL", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8404), "Netherlands" },
                    { 153, "NP", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8402), "Nepal" },
                    { 125, "LY", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8311), "Libyan Arab Jamahiriya" },
                    { 127, "LT", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8315), "Lithuania" },
                    { 128, "LU", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8317), "Luxembourg" },
                    { 129, "MO", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8351), "Macao" },
                    { 130, "MK", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8353), "Macedonia, The Former Yugoslav Republic of" },
                    { 131, "MG", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8355), "Madagascar" },
                    { 132, "MW", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8357), "Malawi" },
                    { 133, "MY", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8360), "Malaysia" },
                    { 134, "MV", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8362), "Maldives" },
                    { 135, "ML", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8365), "Mali" },
                    { 136, "MT", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8367), "Malta" }
                });

            migrationBuilder.InsertData(
                table: "Country",
                columns: new[] { "Id", "Code", "DateCreated", "Name" },
                values: new object[,]
                {
                    { 137, "MH", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8369), "Marshall Islands" },
                    { 138, "MQ", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8371), "Martinique" },
                    { 139, "MR", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8373), "Mauritania" },
                    { 140, "MU", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8375), "Mauritius" },
                    { 141, "YT", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8377), "Mayotte" },
                    { 142, "MX", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8379), "Mexico" },
                    { 143, "FM", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8381), "Micronesia, Federated States of" },
                    { 144, "MD", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8383), "Moldova, Republic of" },
                    { 145, "MC", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8385), "Monaco" },
                    { 146, "MN", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8388), "Mongolia" },
                    { 147, "MS", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8390), "Montserrat" },
                    { 148, "MA", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8392), "Morocco" },
                    { 149, "MZ", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8394), "Mozambique" },
                    { 150, "MM", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8396), "Myanmar" },
                    { 151, "NA", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8398), "Namibia" },
                    { 152, "NR", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8400), "Nauru" },
                    { 183, "SH", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8464), "Saint Helena" },
                    { 124, "LR", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8309), "Liberia" },
                    { 184, "KN", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8466), "Saint Kitts and Nevis" },
                    { 186, "PM", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8470), "Saint Pierre and Miquelon" },
                    { 218, "TK", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8538), "Tokelau" },
                    { 219, "TO", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8540), "Tonga" },
                    { 220, "TT", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8542), "Trinidad and Tobago" },
                    { 221, "TN", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8544), "Tunisia" },
                    { 222, "TR", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8546), "Turkey" },
                    { 223, "TM", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8548), "Turkmenistan" },
                    { 224, "TC", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8550), "Turks and Caicos Islands" },
                    { 225, "TV", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8552), "Tuvalu" },
                    { 226, "UG", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8554), "Uganda" },
                    { 227, "UA", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8556), "Ukraine" },
                    { 228, "AE", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8558), "United Arab Emirates" },
                    { 229, "GB", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8560), "United Kingdom" },
                    { 217, "TG", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8536), "Togo" },
                    { 230, "US", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8562), "United States" },
                    { 232, "UY", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8567), "Uruguay" },
                    { 233, "UZ", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8569), "Uzbekistan" },
                    { 234, "VU", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8571), "Vanuatu" },
                    { 235, "VE", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8573), "Venezuela" },
                    { 236, "VN", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8575), "Viet Nam" },
                    { 237, "VG", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8577), "Virgin Islands, British" },
                    { 238, "VI", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8579), "Virgin Islands, U.S." },
                    { 239, "WF", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8581), "Wallis and Futuna" }
                });

            migrationBuilder.InsertData(
                table: "Country",
                columns: new[] { "Id", "Code", "DateCreated", "Name" },
                values: new object[,]
                {
                    { 240, "EH", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8583), "Western Sahara" },
                    { 241, "YE", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8585), "Yemen" },
                    { 242, "ZM", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8587), "Zambia" },
                    { 243, "ZW", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8589), "Zimbabwe" },
                    { 231, "UM", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8564), "United States Minor Outlying Islands" },
                    { 216, "TL", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8534), "Timor-Leste" },
                    { 215, "TH", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8531), "Thailand" },
                    { 214, "TZ", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8529), "Tanzania, United Republic of" },
                    { 187, "VC", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8473), "Saint Vincent and the Grenadines" },
                    { 188, "WS", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8475), "Samoa" },
                    { 189, "SM", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8477), "San Marino" },
                    { 190, "ST", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8479), "Sao Tome and Principe" },
                    { 191, "SA", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8481), "Saudi Arabia" },
                    { 192, "SN", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8484), "Senegal" },
                    { 193, "CS", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8486), "Serbia and Montenegro" },
                    { 194, "SC", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8488), "Seychelles" },
                    { 195, "SL", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8490), "Sierra Leone" },
                    { 196, "SG", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8492), "Singapore" },
                    { 197, "SK", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8494), "Slovakia" },
                    { 198, "SI", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8496), "Slovenia" },
                    { 199, "SB", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8498), "Solomon Islands" },
                    { 200, "SO", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8501), "Somalia" },
                    { 201, "ZA", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8503), "South Africa" },
                    { 202, "GS", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8505), "South Georgia and the South Sandwich Islands" },
                    { 203, "ES", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8507), "Spain" },
                    { 204, "LK", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8509), "Sri Lanka" },
                    { 205, "SD", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8511), "Sudan" },
                    { 206, "SR", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8513), "SuriName" },
                    { 207, "SJ", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8515), "Svalbard and Jan Mayen" },
                    { 208, "SZ", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8517), "Swaziland" },
                    { 209, "SE", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8519), "Sweden" },
                    { 210, "CH", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8521), "Switzerland" },
                    { 211, "SY", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8523), "Syrian Arab Republic" },
                    { 212, "TW", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8525), "Taiwan, Province of China" },
                    { 213, "TJ", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8527), "Tajikistan" },
                    { 185, "LC", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8468), "Saint Lucia" },
                    { 123, "LS", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8307), "Lesotho" },
                    { 126, "LI", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8313), "Liechtenstein" },
                    { 121, "LV", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8302), "Latvia" },
                    { 33, "BN", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8118), "Brunei Darussalam" },
                    { 34, "BG", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8120), "Bulgaria" },
                    { 35, "BF", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8122), "Burkina Faso" }
                });

            migrationBuilder.InsertData(
                table: "Country",
                columns: new[] { "Id", "Code", "DateCreated", "Name" },
                values: new object[,]
                {
                    { 36, "BI", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8124), "Burundi" },
                    { 37, "KH", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8126), "Cambodia" },
                    { 38, "CM", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8128), "Cameroon" },
                    { 39, "CA", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8130), "Canada" },
                    { 40, "CV", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8132), "Cape Verde" },
                    { 41, "KY", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8134), "Cayman Islands" },
                    { 42, "CF", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8136), "Central African Republic" },
                    { 43, "TD", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8138), "Chad" },
                    { 44, "CL", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8140), "Chile" },
                    { 45, "CN", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8142), "China" },
                    { 46, "CX", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8145), "Christmas Island" },
                    { 47, "CC", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8147), "Cocos (Keeling) Islands" },
                    { 48, "CO", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8149), "Colombia" },
                    { 49, "KM", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8151), "Comoros" },
                    { 50, "CG", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8153), "Congo" },
                    { 51, "CD", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8156), "Congo, The Democratic Republic of the" },
                    { 52, "CK", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8158), "Cook Islands" },
                    { 53, "CR", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8160), "Costa Rica" },
                    { 54, "CI", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8162), "Cote D\"Ivoire" },
                    { 55, "HR", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8164), "Croatia" },
                    { 56, "CU", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8166), "Cuba" },
                    { 57, "CY", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8168), "Cyprus" },
                    { 122, "LB", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8305), "Lebanon" },
                    { 59, "DK", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8172), "Denmark" },
                    { 32, "IO", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8115), "British Indian Ocean Territory" },
                    { 60, "DJ", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8175), "Djibouti" },
                    { 31, "BR", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8113), "Brazil" },
                    { 29, "BW", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8109), "Botswana" },
                    { 2, "AX", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8028), "Åland Islands" },
                    { 3, "AL", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8053), "Albania" },
                    { 4, "DZ", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8055), "Algeria" },
                    { 5, "AS", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8057), "American Samoa" },
                    { 6, "AD", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8060), "AndorrA" },
                    { 7, "AO", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8062), "Angola" },
                    { 8, "AI", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8064), "Anguilla" },
                    { 9, "AQ", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8067), "Antarctica" },
                    { 10, "AG", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8069), "Antigua and Barbuda" },
                    { 11, "AR", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8071), "Argentina" },
                    { 12, "AM", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8073), "Armenia" },
                    { 13, "AW", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8075), "Aruba" },
                    { 14, "AU", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8077), "Australia" },
                    { 15, "AT", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8079), "Austria" }
                });

            migrationBuilder.InsertData(
                table: "Country",
                columns: new[] { "Id", "Code", "DateCreated", "Name" },
                values: new object[,]
                {
                    { 16, "AZ", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8081), "Azerbaijan" },
                    { 17, "BS", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8084), "Bahamas" },
                    { 18, "BH", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8086), "Bahrain" },
                    { 19, "BD", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8088), "Bangladesh" },
                    { 20, "BB", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8090), "Barbados" },
                    { 21, "BY", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8092), "Belarus" },
                    { 22, "BE", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8094), "Belgium" },
                    { 23, "BZ", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8097), "Belize" },
                    { 24, "BJ", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8099), "Benin" },
                    { 25, "BM", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8101), "Bermuda" },
                    { 26, "BT", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8103), "Bhutan" },
                    { 27, "BO", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8105), "Bolivia" },
                    { 28, "BA", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8107), "Bosnia and Herzegovina" },
                    { 30, "BV", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8111), "Bouvet Island" },
                    { 61, "DM", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8177), "Dominica" },
                    { 58, "CZ", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8170), "Czech Republic" },
                    { 63, "EC", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8181), "Ecuador" },
                    { 95, "HM", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8248), "Heard Island and Mcdonald Islands" },
                    { 62, "DO", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8179), "Dominican Republic" },
                    { 97, "HN", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8252), "Honduras" },
                    { 98, "HK", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8254), "Hong Kong" },
                    { 99, "HU", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8256), "Hungary" },
                    { 100, "IS", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8258), "Iceland" },
                    { 101, "IN", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8260), "India" },
                    { 102, "ID", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8262), "Indonesia" },
                    { 103, "IR", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8265), "Iran, Islamic Republic Of" },
                    { 104, "IQ", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8267), "Iraq" },
                    { 105, "IE", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8269), "Ireland" },
                    { 106, "IM", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8271), "Isle of Man" },
                    { 94, "HT", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8246), "Haiti" },
                    { 107, "IL", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8273), "Israel" },
                    { 109, "JM", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8277), "Jamaica" },
                    { 110, "JP", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8279), "Japan" },
                    { 111, "JE", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8282), "Jersey" },
                    { 112, "JO", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8284), "Jordan" },
                    { 113, "KZ", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8286), "Kazakhstan" },
                    { 114, "KE", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8288), "Kenya" },
                    { 115, "KI", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8290), "Kiribati" },
                    { 116, "KP", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8292), "Korea, Democratic People\"S Republic of" },
                    { 117, "KR", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8294), "Korea, Republic of" },
                    { 118, "KW", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8296), "Kuwait" },
                    { 119, "KG", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8298), "Kyrgyzstan" }
                });

            migrationBuilder.InsertData(
                table: "Country",
                columns: new[] { "Id", "Code", "DateCreated", "Name" },
                values: new object[,]
                {
                    { 120, "LA", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8300), "Lao People\"S Democratic Republic" },
                    { 108, "IT", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8275), "Italy" },
                    { 93, "GY", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8244), "Guyana" },
                    { 96, "VA", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8250), "Holy See (Vatican City State)" },
                    { 91, "GN", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8240), "Guinea" },
                    { 64, "EG", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8183), "Egypt" },
                    { 65, "SV", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8185), "El Salvador" },
                    { 66, "GQ", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8187), "Equatorial Guinea" },
                    { 67, "ER", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8189), "Eritrea" },
                    { 68, "EE", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8191), "Estonia" },
                    { 69, "ET", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8193), "Ethiopia" },
                    { 70, "FK", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8195), "Falkland Islands (Malvinas)" },
                    { 71, "FO", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8197), "Faroe Islands" },
                    { 72, "FJ", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8199), "Fiji" },
                    { 92, "GW", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8242), "Guinea-Bissau" },
                    { 74, "FR", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8203), "France" },
                    { 75, "GF", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8205), "French Guiana" },
                    { 76, "PF", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8208), "French Polynesia" },
                    { 73, "FI", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8201), "Finland" },
                    { 78, "GA", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8212), "Gabon" },
                    { 90, "GG", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8238), "Guernsey" },
                    { 89, "GT", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8236), "Guatemala" },
                    { 77, "TF", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8210), "French Southern Territories" },
                    { 88, "GU", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8233), "Guam" },
                    { 86, "GD", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8229), "Grenada" },
                    { 85, "GL", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8227), "Greenland" },
                    { 87, "GP", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8231), "Guadeloupe" },
                    { 83, "GI", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8223), "Gibraltar" },
                    { 82, "GH", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8221), "Ghana" },
                    { 81, "DE", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8219), "Germany" },
                    { 80, "GE", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8216), "Georgia" },
                    { 79, "GM", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8214), "Gambia" },
                    { 84, "GR", new DateTime(2020, 11, 22, 12, 42, 20, 53, DateTimeKind.Local).AddTicks(8225), "Greece" }
                });

            migrationBuilder.InsertData(
                table: "ParkingConfiguration",
                columns: new[] { "Id", "DateCreated", "TotalParking" },
                values: new object[] { 1, new DateTime(2020, 11, 22, 12, 42, 20, 34, DateTimeKind.Local).AddTicks(1608), 30 });

            migrationBuilder.InsertData(
                table: "PhoneType",
                columns: new[] { "Id", "DateCreated", "Type" },
                values: new object[,]
                {
                    { 1, new DateTime(2020, 11, 22, 12, 42, 20, 56, DateTimeKind.Local).AddTicks(2044), "Comercial" },
                    { 2, new DateTime(2020, 11, 22, 12, 42, 20, 56, DateTimeKind.Local).AddTicks(2626), "Residencial" },
                    { 3, new DateTime(2020, 11, 22, 12, 42, 20, 56, DateTimeKind.Local).AddTicks(2640), "Celular" }
                });

            migrationBuilder.InsertData(
                table: "User",
                columns: new[] { "Id", "DateCreated", "Password", "UserName" },
                values: new object[] { 1, new DateTime(2020, 11, 22, 12, 42, 20, 27, DateTimeKind.Local).AddTicks(5717), "103a13bc1176e35c32446f149fc760d079e104ed464e3cf1b3657081ed147abb7dcb861b8fd01646e951cd15615d4e6c04563524f485a27ed0ab689b715cdfff", "Admin" });

            migrationBuilder.InsertData(
                table: "VehicleType",
                columns: new[] { "Id", "DateCreated", "Type" },
                values: new object[,]
                {
                    { 2, new DateTime(2020, 11, 22, 12, 42, 20, 59, DateTimeKind.Local).AddTicks(1639), "Automóvel" },
                    { 3, new DateTime(2020, 11, 22, 12, 42, 20, 59, DateTimeKind.Local).AddTicks(1665), "Microônibus" },
                    { 1, new DateTime(2020, 11, 22, 12, 42, 20, 59, DateTimeKind.Local).AddTicks(1136), "Motocicleta" },
                    { 4, new DateTime(2020, 11, 22, 12, 42, 20, 59, DateTimeKind.Local).AddTicks(1667), "Ônibus" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Address_CompanyId",
                table: "Address",
                column: "CompanyId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Address_CountryId",
                table: "Address",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_CompanyConfiguration_CompanyId",
                table: "CompanyConfiguration",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_CompanyConfiguration_VehicleTypeId",
                table: "CompanyConfiguration",
                column: "VehicleTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_ParkingManagement_VehicleId",
                table: "ParkingManagement",
                column: "VehicleId");

            migrationBuilder.CreateIndex(
                name: "IX_Phone_CompanyId",
                table: "Phone",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Phone_PhoneTypeId",
                table: "Phone",
                column: "PhoneTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Vehicle_CompanyId",
                table: "Vehicle",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Vehicle_VehicleTypeId",
                table: "Vehicle",
                column: "VehicleTypeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Address");

            migrationBuilder.DropTable(
                name: "CompanyConfiguration");

            migrationBuilder.DropTable(
                name: "ParkingConfiguration");

            migrationBuilder.DropTable(
                name: "ParkingManagement");

            migrationBuilder.DropTable(
                name: "Phone");

            migrationBuilder.DropTable(
                name: "User");

            migrationBuilder.DropTable(
                name: "Country");

            migrationBuilder.DropTable(
                name: "Vehicle");

            migrationBuilder.DropTable(
                name: "PhoneType");

            migrationBuilder.DropTable(
                name: "Company");

            migrationBuilder.DropTable(
                name: "VehicleType");
        }
    }
}
