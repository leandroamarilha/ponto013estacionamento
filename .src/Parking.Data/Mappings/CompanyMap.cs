﻿using Parking.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Parking.Data.Mappings
{
    public class CompanyMap : IEntityTypeConfiguration<Company>
    {
        public void Configure(EntityTypeBuilder<Company> builder)
        {
            #region Table Definitions

            builder.ToTable("Company");
            builder.HasKey(x => x.Id);

            #endregion

            #region Properties

            builder.Property(x => x.Id)
                   .ValueGeneratedOnAdd();

            builder.Property(x => x.Name)
                   .HasColumnType("varchar(200)")
                   .IsRequired();

            builder.Property(x => x.CNPJ)
                   .HasColumnType("varchar(14)")
                   .IsRequired();

            builder.Property(x => x.DateCreated)
                   .IsRequired();

            #endregion

            #region Relationships

            builder.HasOne(x => x.Address)
                   .WithOne(x => x.Company)
                   .HasForeignKey<Address>(x => x.CompanyId);

            builder.HasMany(x => x.Phones)
                   .WithOne(x => x.Company)
                   .HasForeignKey(x => x.CompanyId);

            builder.HasMany(x => x.CompanyConfigurations)
                   .WithOne(x => x.Company)
                   .HasForeignKey(x => x.CompanyId);

            #endregion

            #region Data

            #endregion
        }
    }
}
