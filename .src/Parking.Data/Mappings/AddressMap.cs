﻿using Parking.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Parking.Data.Mappings
{
    public class AddressMap : IEntityTypeConfiguration<Address>
    {
        public void Configure(EntityTypeBuilder<Address> builder)
        {
            #region Table Definitions

            builder.ToTable("Address");
            builder.HasKey(x => x.Id);

            #endregion

            #region Properties

            builder.Property(x => x.Id)
                   .ValueGeneratedOnAdd();

            builder.Property(x => x.Name)
                   .HasColumnType("varchar(200)")
                   .IsRequired();

            builder.Property(x => x.Number)
                   .HasColumnType("varchar(30)")
                   .IsRequired();

            builder.Property(x => x.Complement)
                   .HasColumnType("varchar(150)")
                   .IsRequired();

            builder.Property(x => x.City)
                   .HasColumnType("varchar(100)")
                   .IsRequired();

            builder.Property(x => x.State)
                   .HasColumnType("varchar(100)")
                   .IsRequired();

            builder.Property(x => x.ZipCode)
                   .HasColumnType("varchar(8)")
                   .IsRequired();

            builder.Property(x => x.DateCreated)
                   .IsRequired();

            #endregion

            #region Relationships

            #endregion

            #region Data

            #endregion
        }
    }
}
