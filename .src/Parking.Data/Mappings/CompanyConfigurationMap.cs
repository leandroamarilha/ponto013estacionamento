﻿using Parking.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Parking.Data.Mappings
{
    public class CompanyConfigurationMap : IEntityTypeConfiguration<CompanyConfiguration>
    {
        public void Configure(EntityTypeBuilder<CompanyConfiguration> builder)
        {
            #region Table Definitions

            builder.ToTable("CompanyConfiguration");
            builder.HasKey(x => x.Id);

            #endregion

            #region Properties

            builder.Property(x => x.Id)
                   .ValueGeneratedOnAdd();

            builder.Property(x => x.ParkingGarageTotal)
                   .IsRequired();

            builder.Property(x => x.VehicleTypeId)
                   .IsRequired();

            builder.Property(x => x.DateCreated)
                   .IsRequired();

            #endregion

            #region Relationships

            #endregion

            #region Data

            #endregion
        }
    }
}
