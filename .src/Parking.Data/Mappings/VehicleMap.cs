﻿using Parking.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Parking.Data.Mappings
{
    public class VehicleMap : IEntityTypeConfiguration<Vehicle>
    {
        public void Configure(EntityTypeBuilder<Vehicle> builder)
        {
            #region Table Definitions

            builder.ToTable("Vehicle");
            builder.HasKey(x => x.Id);

            #endregion

            #region Properties

            builder.Property(x => x.Id)
                   .ValueGeneratedOnAdd();

            builder.Property(x => x.Brand)
                   .HasColumnType("varchar(100)")
                   .IsRequired();

            builder.Property(x => x.Model)
                   .HasColumnType("varchar(100)")
                   .IsRequired();

            builder.Property(x => x.Color)
                   .HasColumnType("varchar(50)")
                   .IsRequired();

            builder.Property(x => x.LicensePlate)
                   .HasColumnType("varchar(7)")
                   .IsRequired();

            builder.Property(x => x.VehicleTypeId)
                   .IsRequired();

            builder.Property(x => x.DateCreated)
                   .IsRequired();

            #endregion

            #region Relationships

            #endregion

            #region Data

            #endregion
        }
    }
}
