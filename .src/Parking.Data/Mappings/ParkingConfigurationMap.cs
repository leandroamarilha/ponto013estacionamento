﻿using Parking.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Parking.Data.Mappings
{
    public class ParkingConfigurationMap : IEntityTypeConfiguration<ParkingConfiguration>
    {
        public void Configure(EntityTypeBuilder<ParkingConfiguration> builder)
        {
            #region Table Definitions

            builder.ToTable("ParkingConfiguration");
            builder.HasKey(x => x.Id);

            #endregion

            #region Properties

            builder.Property(x => x.Id)
                   .ValueGeneratedOnAdd();

            builder.Property(x => x.TotalParking)
                   .IsRequired();

            builder.Property(x => x.DateCreated)
                   .IsRequired();

            #endregion

            #region Relationships

            #endregion

            #region Data

            builder.HasData(
                new ParkingConfiguration
                {
                    Id = 1,
                    TotalParking = 30
                }
            );

            #endregion
        }
    }
}
