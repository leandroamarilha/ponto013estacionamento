﻿using Parking.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Parking.Data.Mappings
{
    public class VehicleTypeMap : IEntityTypeConfiguration<VehicleType>
    {
        public void Configure(EntityTypeBuilder<VehicleType> builder)
        {
            #region Table Definitions

            builder.ToTable("VehicleType");
            builder.HasKey(x => x.Id);

            #endregion

            #region Properties

            builder.Property(x => x.Id)
                   .ValueGeneratedOnAdd();

            builder.Property(x => x.Type)
                   .HasColumnType("varchar(50)")
                   .IsRequired();

            builder.Property(x => x.DateCreated)
                   .IsRequired();

            #endregion

            #region Relationships

            #endregion

            #region Data

            builder.HasData(
                new VehicleType
                {
                    Id = 1,
                    Type = "Motocicleta"
                },
                new VehicleType
                {
                    Id = 2,
                    Type = "Automóvel"
                },
                new VehicleType
                {
                    Id = 3,
                    Type = "Microônibus"
                },
                new VehicleType
                {
                    Id = 4,
                    Type = "Ônibus"
                }
            );

            #endregion
        }
    }
}
