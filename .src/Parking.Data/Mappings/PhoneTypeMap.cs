﻿using Parking.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Parking.Data.Mappings
{
    public class PhoneTypeMap : IEntityTypeConfiguration<PhoneType>
    {
        public void Configure(EntityTypeBuilder<PhoneType> builder)
        {
            #region Table Definitions

            builder.ToTable("PhoneType");
            builder.HasKey(x => x.Id);

            #endregion

            #region Properties

            builder.Property(x => x.Id)
                   .ValueGeneratedOnAdd();

            builder.Property(x => x.Type)
                   .HasColumnType("varchar(50)")
                   .IsRequired();

            builder.Property(x => x.DateCreated)
                   .IsRequired();

            #endregion

            #region Relationships

            #endregion

            #region Data

            builder.HasData(
                new PhoneType
                {
                    Id = 1,
                    Type = "Comercial"
                },
                new PhoneType
                {
                    Id = 2,
                    Type = "Residencial"
                },
                new PhoneType
                {
                    Id = 3,
                    Type = "Celular"
                }
            );

            #endregion
        }
    }
}
