﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Parking.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Parking.Data.Mappings
{
    public class ParkingManagementMap : IEntityTypeConfiguration<ParkingManagement>
    {
        public void Configure(EntityTypeBuilder<ParkingManagement> builder)
        {
            #region Table Definitions

            builder.ToTable("ParkingManagement");
            builder.HasKey(x => x.Id);

            #endregion

            #region Properties

            builder.Property(x => x.Id)
                   .ValueGeneratedOnAdd();

            builder.Property(x => x.DateTimeEntry)
                   .IsRequired();

            builder.Property(x => x.DateCreated)
                   .IsRequired();

            #endregion

            #region Relationships

            #endregion

            #region Data

            #endregion
        }
    }
}
