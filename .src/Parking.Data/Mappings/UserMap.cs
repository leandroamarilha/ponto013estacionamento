﻿using Parking.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Parking.CrossCutting.Security.Encryption;

namespace Parking.Data.Mappings
{
    public class UserMap : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            #region Table Definitions

            builder.ToTable("User");
            builder.HasKey(x => x.Id);

            #endregion

            #region Properties

            builder.Property(x => x.Id)
                   .ValueGeneratedOnAdd();

            builder.Property(x => x.UserName)
                   .HasColumnType("varchar(100)")
                   .IsRequired();

            builder.Property(x => x.Password)
                   .HasColumnType("varchar(200)")
                   .IsRequired();

            builder.Property(x => x.DateCreated)
                   .IsRequired();

            #endregion

            #region Relationships

            #endregion

            #region Data

            builder.HasData(
                new User
                {
                    Id = 1,
                    UserName = "Admin",
                    Password = UtilEncryption.HashHmac("Admin" + "!@#$%QWE", "123456")
                }
            );

            #endregion
        }
    }
}
