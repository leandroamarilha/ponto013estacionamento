﻿using Microsoft.EntityFrameworkCore;
using Parking.Data.Contexts;
using Parking.Domain.Entities;
using Parking.Domain.Interfaces.Repository;
using System.Threading.Tasks;

namespace Parking.Data.Repository
{
    public class PhoneTypeRepository : Repository<PhoneType>, IPhoneTypeRepository
    {
        public PhoneTypeRepository(DataContext context) : base(context)
        {
        }

        public async Task<PhoneType> GetFullById(int id)
        {
            return await _db.PhoneType
                            .AsNoTracking()
                            .FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}
