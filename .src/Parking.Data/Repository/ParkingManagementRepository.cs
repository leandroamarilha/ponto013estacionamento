﻿using Microsoft.EntityFrameworkCore;
using Parking.Data.Contexts;
using Parking.Domain.Entities;
using Parking.Domain.Interfaces.Repository;
using System.Threading.Tasks;
using System.Data;
using System.Linq;

namespace Parking.Data.Repository
{
    public class ParkingManagementRepository : Repository<ParkingManagement>, IParkingManagementRepository
    {
        public ParkingManagementRepository(DataContext context) : base(context)
        {

        }

        public async Task<bool> VehicleInTheParking(string licensePlate)
        {
            return await _db.ParkingManagement
                            .Include(x => x.Vehicle)
                            .AsNoTracking()
                            .AnyAsync(x => x.Vehicle.LicensePlate == licensePlate
                                   && x.DateTimeEntry != null
                                   && x.DateTimeExit == null);
        }

        public async Task<ParkingManagement> GetFullById(int id)
        {
            return await _db.ParkingManagement
                            .Include(x => x.Vehicle)
                            .ThenInclude(x => x.VehicleType)
                            .Include(x => x.Vehicle)
                            .ThenInclude(x => x.Company)
                            .AsNoTracking()
                            .FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<ParkingManagement> GetFullByLicensePlate(string licensePlate)
        {
            return await _db.ParkingManagement
                            .Include(x => x.Vehicle)
                            .ThenInclude(x => x.VehicleType)
                            .Include(x => x.Vehicle)
                            .ThenInclude(x => x.Company)
                            .AsNoTracking()
                            .FirstOrDefaultAsync(x => x.Vehicle.LicensePlate == licensePlate);
        }

        public async Task<int> GetParkingAvailable (int companyId, int vehicleTypeId)
        {
            var totalVehicleCompany = await _db.CompanyConfiguration
                                        .Where(x => x.CompanyId == companyId && x.VehicleTypeId == vehicleTypeId)
                                        .AsNoTracking()
                                        .Select(x => x.ParkingGarageTotal)
                                        .SumAsync();

            var TotalVehiclesInTheParking = await _db.ParkingManagement
                                                     .Include(x => x.Vehicle)
                                                     .ThenInclude(x => x.VehicleType)
                                                     .AsNoTracking()
                                                     .CountAsync(x => x.DateTimeEntry != null && x.DateTimeExit == null);

            return totalVehicleCompany - TotalVehiclesInTheParking;
        }
    }
}
