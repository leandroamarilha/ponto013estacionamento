﻿using Microsoft.EntityFrameworkCore;
using Parking.Data.Contexts;
using Parking.Domain.Entities;
using Parking.Domain.Interfaces.Repository;
using System.Threading.Tasks;

namespace Parking.Data.Repository
{
    public class VehicleRepository : Repository<Vehicle>, IVehicleRepository
    {
        public VehicleRepository(DataContext context) : base(context)
        {
        }

        public async Task<Vehicle> GetFullById(int id)
        {
            return await _db.Vehicle
                            .Include(x => x.VehicleType)
                            .Include(x => x.Company)
                            .AsNoTracking()
                            .FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<Vehicle> GetVehicleByLicensePlate(string licensePlate)
        {
            return await _db.Vehicle
                            .Include(x => x.VehicleType)
                            .AsNoTracking()
                            .FirstOrDefaultAsync(x => x.LicensePlate == licensePlate);
        }

        public async Task<bool> ExistByLicensePlate(string licensePlate)
        {
            return await _db.Vehicle.AnyAsync(x => x.LicensePlate == licensePlate);
        }
    }
}
