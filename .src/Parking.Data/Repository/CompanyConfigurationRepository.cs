﻿using Microsoft.EntityFrameworkCore;
using Parking.Data.Contexts;
using Parking.Domain.Entities;
using Parking.Domain.Interfaces.Repository;
using System.Threading.Tasks;

namespace Parking.Data.Repository
{
    public class CompanyConfigurationRepository : Repository<CompanyConfiguration>, ICompanyConfigurationRepository
    {
        public CompanyConfigurationRepository(DataContext context) : base(context)
        {
        }

        public async Task<CompanyConfiguration> GetFullById(int id)
        {
            return await _db.CompanyConfiguration
                            .Include(x => x.Company)
                            .Include(x => x.VehicleType)
                            .AsNoTracking()
                            .FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}
