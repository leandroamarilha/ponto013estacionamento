﻿using Microsoft.EntityFrameworkCore;
using Parking.Data.Contexts;
using Parking.Domain.Entities;
using Parking.Domain.Interfaces.Repository;
using System.Threading.Tasks;

namespace Parking.Data.Repository
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(DataContext context) : base(context)
        {
        }

        #region Login

        public async Task<User> VerifyLogin(User user)
        {
            return await _db.User.FirstOrDefaultAsync(x => x.UserName == user.UserName);
        }

        #endregion

        public async Task<User> GetFullById(int id)
        {
            return await _db.User
                            .AsNoTracking()
                            .FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}
