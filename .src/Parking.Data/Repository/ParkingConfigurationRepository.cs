﻿using Microsoft.EntityFrameworkCore;
using Parking.Data.Contexts;
using Parking.Domain.Entities;
using Parking.Domain.Interfaces.Repository;
using System.Threading.Tasks;

namespace Parking.Data.Repository
{
    public class ParkingConfigurationRepository : Repository<ParkingConfiguration>, IParkingConfigurationRepository
    {
        public ParkingConfigurationRepository(DataContext context) : base(context)
        {

        }

        public async Task<ParkingConfiguration> GetFullById(int id)
        {
            return await _db.ParkingConfiguration
                            .AsNoTracking()
                            .FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<int> GetParkingTotal()
        {
            return await _db.ParkingConfiguration.SumAsync(x => x.TotalParking);
        }
    }
}
