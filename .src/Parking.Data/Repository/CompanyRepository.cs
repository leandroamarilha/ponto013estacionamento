﻿using Microsoft.EntityFrameworkCore;
using Parking.Data.Contexts;
using Parking.Domain.Entities;
using Parking.Domain.Interfaces.Repository;
using System.Threading.Tasks;

namespace Parking.Data.Repository
{
    public class CompanyRepository : Repository<Company>, ICompanyRepository
    {
        public CompanyRepository(DataContext context) : base(context)
        {            
        }

        public async Task<Company> GetFullById(int id)
        {
            return await _db.Company
                            .Include(x => x.Address)
                            .Include(x => x.Address.Country)
                            .Include(x => x.CompanyConfigurations)
                            .ThenInclude(x => x.VehicleType)
                            .Include(x => x.Phones)
                            .ThenInclude(x => x.PhoneType)
                            .AsNoTracking()
                            .FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}
