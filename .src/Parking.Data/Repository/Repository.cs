﻿using Microsoft.EntityFrameworkCore;
using Parking.Data.Contexts;
using Parking.Domain.Entities;
using Parking.Domain.Interfaces.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Parking.Data.Repository
{
    public abstract class Repository<TEntity> : IRepository<TEntity> where TEntity : EntityBase, new()
    {
        protected readonly DataContext _db;
        protected readonly DbSet<TEntity> _dbSet;

        public Repository(DataContext db)
        {
            _db = db;
            _dbSet = db.Set<TEntity>();
        }

        public async Task Insert(TEntity entity)
        {
            _dbSet.Add(entity);
            await SaveChanges();
        }

        public async Task<TEntity> InsertWithReturn(TEntity entity)
        {
            _dbSet.Add(entity);
            await SaveChanges();
            return entity;
        }

        public async Task Update(TEntity entity)
        {
            _dbSet.Update(entity);
            await SaveChanges();
        }

        public async Task<TEntity> UpdateWithReturn(TEntity entity)
        {
            _dbSet.Update(entity);
            await SaveChanges();
            return entity;
        }

        public virtual async Task Delete(int id)
        {
            var entity = new TEntity { Id = id };
            _dbSet.Remove(entity);
            await SaveChanges();
        }

        public async Task<bool> Exist(int id)
        {
            var entity = new TEntity { Id = id };
            return await _dbSet.AnyAsync(x => x.Id == entity.Id);
        }

        public async Task<TEntity> GetById(int id)
        {
            return await _dbSet.FindAsync(id);
        }

        public async Task<List<TEntity>> GetAll()
        {
            return await _dbSet.ToListAsync();
        }

        public async Task<IEnumerable<TEntity>> Search(Expression<Func<TEntity, bool>> predicate)
        {
            return await _dbSet.AsNoTracking().Where(predicate).ToListAsync();
        }

        public async Task<int> SaveChanges()
        {
            return await _db.SaveChangesAsync();
        }

        public void Dispose()
        {
            _db?.Dispose();
        }
    }
}
