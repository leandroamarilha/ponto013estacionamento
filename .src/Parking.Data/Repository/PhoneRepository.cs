﻿using Microsoft.EntityFrameworkCore;
using Parking.Data.Contexts;
using Parking.Domain.Entities;
using Parking.Domain.Interfaces.Repository;
using System.Threading.Tasks;

namespace Parking.Data.Repository
{
    public class PhoneRepository : Repository<Phone>, IPhoneRepository
    {
        public PhoneRepository(DataContext context) : base(context)
        {
        }

        public async Task<Phone> GetFullById(int id)
        {
            return await _db.Phone
                            .Include(x => x.PhoneType)
                            .Include(x => x.Company)
                            .AsNoTracking()
                            .FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}
