﻿using Microsoft.EntityFrameworkCore;
using Parking.Data.Contexts;
using Parking.Domain.Entities;
using Parking.Domain.Interfaces.Repository;
using System.Threading.Tasks;

namespace Parking.Data.Repository
{
    public class VehicleTypeRepository : Repository<VehicleType>, IVehicleTypeRepository
    {
        public VehicleTypeRepository(DataContext context) : base(context)
        {
        }

        public async Task<VehicleType> GetFullById(int id)
        {
            return await _db.VehicleType
                            .AsNoTracking()
                            .FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}
