﻿using Parking.Data.Mappings;
using Parking.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Parking.Data.Contexts
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) {}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            
            modelBuilder.ApplyConfiguration(new UserMap());
            modelBuilder.ApplyConfiguration(new ParkingConfigurationMap());
            modelBuilder.ApplyConfiguration(new CompanyMap());
            modelBuilder.ApplyConfiguration(new CompanyConfigurationMap());            
            modelBuilder.ApplyConfiguration(new AddressMap());            
            modelBuilder.ApplyConfiguration(new CountryMap());         
            modelBuilder.ApplyConfiguration(new PhoneMap());
            modelBuilder.ApplyConfiguration(new PhoneTypeMap());
            modelBuilder.ApplyConfiguration(new VehicleMap());
            modelBuilder.ApplyConfiguration(new VehicleTypeMap());
            modelBuilder.ApplyConfiguration(new ParkingManagementMap());

            base.OnModelCreating(modelBuilder);
        }

        public DbSet<User> User { get; set; }
        public DbSet<ParkingConfiguration> ParkingConfiguration { get; set; }
        public DbSet<Company> Company { get; set; }
        public DbSet<CompanyConfiguration> CompanyConfiguration { get; set; }
        public DbSet<Address> Address { get; set; }
        public DbSet<Country> Country { get; set; }
        public DbSet<Phone> Phone { get; set; }
        public DbSet<PhoneType> PhoneType { get; set; }
        public DbSet<Vehicle> Vehicle { get; set; }
        public DbSet<VehicleType> VehicleType { get; set; }
        public DbSet<ParkingManagement> ParkingManagement { get; set; }
    }
}
