﻿//using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Parking.Application.Configurations;
using Parking.Application.Interfaces;
using Parking.Application.Interfaces.Security;
using Parking.Application.Security;
using Parking.Application.Services;
using Parking.Data.Contexts;
using Parking.Data.Repository;
using Parking.Domain.Interfaces.Notifications;
using Parking.Domain.Interfaces.Repository;
using Parking.Domain.Interfaces.Services;
using Parking.Domain.Notifications;
using Parking.Domain.Services;

namespace Parking.CrossCutting.DependencyInjection
{
    public static class CustomService
    {
        public static void AddCustomService(this IServiceCollection services, IConfiguration configuration)
        {
            #region Conexões/Contextos
            
            services.AddDbContext<DataContext>(s => s.UseSqlServer(configuration.GetConnectionString("DataConnection")))
                    .AddDbContext<DataContext>();

            #endregion

            #region Injeções Serviços

            services.AddScoped<ICompanyAppService, CompanyAppService>()
                    .AddScoped<IVehicleAppService, VehicleAppService>()
                    .AddScoped<IUserAppService, UserAppService>()
                    .AddScoped<IParkingConfigurationAppService, ParkingConfigurationAppService>()
                    .AddScoped<IParkingManagementAppService, ParkingManagementAppService>()
                    .AddScoped<ICompanyService, CompanyService>()
                    .AddScoped<IVehicleService, VehicleService>()
                    .AddScoped<IParkingConfigurationService, ParkingConfigurationService>()
                    .AddScoped<IParkingManagementService, ParkingManagementService>()
                    .AddScoped<IUserService, UserService>()
                    .AddScoped<INotifier, Notifier>()
                    .AddScoped<ITokenSecurity>(ctro => new TokenSecurity(configuration.GetSection(nameof(TokenConfiguration)).Get<TokenConfiguration>()));

            #endregion

            #region Injeções Repositórios

            services.AddScoped<ICompanyRepository, CompanyRepository>()
                    .AddScoped<ICompanyConfigurationRepository, CompanyConfigurationRepository>()
                    .AddScoped<IAddressRepository, AddressRepository>()
                    .AddScoped<ICountryRepository, CountryRepository>()
                    .AddScoped<IPhoneRepository, PhoneRepository>()
                    .AddScoped<IPhoneTypeRepository, PhoneTypeRepository>()
                    .AddScoped<IVehicleRepository, VehicleRepository>()
                    .AddScoped<IVehicleTypeRepository, VehicleTypeRepository>()
                    .AddScoped<IUserRepository, UserRepository>()
                    .AddScoped<IParkingConfigurationRepository, ParkingConfigurationRepository>()
                    .AddScoped<IParkingManagementRepository, ParkingManagementRepository>();

            #endregion
        }
    }
}
