﻿using AutoMapper;
using Parking.Application.Interfaces.ViewsModels;
using Parking.Application.ViewsModels;
using Parking.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Parking.CrossCutting.AutoMapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<User, IUserLoginViewModel>().ReverseMap();
            CreateMap<Company, CompanyViewModel>().ReverseMap();
            CreateMap<CompanyConfiguration, CompanyConfigurationViewModel>().ReverseMap();
            CreateMap<Address, AddressViewModel>().ReverseMap();
            CreateMap<Phone, PhoneViewModel>().ReverseMap();
            CreateMap<PhoneType, PhoneTypeViewModel>().ReverseMap();
            CreateMap<Country, CountryViewModel>().ReverseMap();
            CreateMap<Vehicle, VehicleViewModel>().ReverseMap();
            CreateMap<VehicleType, VehicleTypeViewModel>().ReverseMap();
            CreateMap<User, UserViewModel>().ReverseMap();
            CreateMap<ParkingConfiguration, ParkingConfigurationViewModel>().ReverseMap();
            CreateMap<ParkingManagement, ParkingManagementViewModel>().ReverseMap();
        }
    }
}
