﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Parking.CrossCutting.AutoMapper
{
    public static class CustomAutoMapping
    {
        public static void AddCustomAutoMapping(this IServiceCollection services, IConfiguration configuration)
        {
            var mappingConfig = new MapperConfiguration(x =>
            {
                x.AddProfile(new MappingProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
        }
    }
}
