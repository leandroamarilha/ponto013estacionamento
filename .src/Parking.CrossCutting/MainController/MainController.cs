﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Parking.Domain.Interfaces.Notifications;
using Parking.Domain.Notifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Parking.CrossCutting.MainController
{
    [ApiController]
    public abstract class MainController : ControllerBase
    {
        private readonly INotifier _notifier;
        protected int ErrorHttp;

        public MainController(INotifier notifier)
        {
            _notifier = notifier;
        }

        protected bool OperationValid()
        {
            return !_notifier.HasNotification();
        }

        protected ActionResult CustomResponse(object result = null)
        {
            if (OperationValid())
            {
                return Ok(new
                {
                    success = true,
                    data = result,
                    errors = Enumerable.Empty<string>()
                });
            }

            var errors = _notifier.GetNotifications().Select(n => n.Message);

            if (ErrorHttp > 0)
            {
                return StatusCode(ErrorHttp, new
                {
                    success = false,
                    data = Enumerable.Empty<string>(),
                    errors
                });
            }

            return BadRequest(new
            {
                success = false,
                data = Enumerable.Empty<string>(),
                errors
            });
        }

        protected ActionResult CustomResponse(ModelStateDictionary modelState)
        {
            if (!modelState.IsValid) NotifierErroModelInvalid(modelState);
            return CustomResponse();
        }

        protected void NotifierErroModelInvalid(ModelStateDictionary modelState)
        {
            var errors = modelState.Values.SelectMany(e => e.Errors);
            foreach (var error in errors)
            {
                var errorMsg = error.Exception == null ? error.ErrorMessage : error.Exception.Message;
                NotifierError(errorMsg);
            }
        }

        protected void NotifierError(string error)
        {
            _notifier.Handle(new Notification(error));
        }
    }
}
