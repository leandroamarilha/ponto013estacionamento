﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Parking.CrossCutting.Helpers
{
    public class LicensePlateValidation
    {
        public static bool Validate(string licensePlate)
        {
            if (string.IsNullOrWhiteSpace(licensePlate)) { return false; }

            if (licensePlate.Length > 8) { return false; }

            licensePlate = licensePlate.Replace("-", "").Trim();

            #region Mercosul Plate

            if (char.IsLetter(licensePlate, 4))
            {
                var mercosulPlate = new Regex("[a-zA-Z]{3}[0-9]{1}[a-zA-Z]{1}[0-9]{2}");
                return mercosulPlate.IsMatch(licensePlate);
            }

            #endregion

            #region Standard Plate

            var standardPlate = new Regex("[a-zA-Z]{3}[0-9]{4}");
            return standardPlate.IsMatch(licensePlate);

            #endregion
        }
    }
}
