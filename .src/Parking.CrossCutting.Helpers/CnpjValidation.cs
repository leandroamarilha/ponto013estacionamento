﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Parking.CrossCutting.Helpers
{
    public class CnpjValidation
    {
        public const int CnpjSize = 14;

        public static bool Validate(string cpnj)
        {
            var cnpjNumeros = Helpers.OnlyNumbers(cpnj);

            if (!HasValidSize(cnpjNumeros)) return false;

            return !HasRepeatedDigits(cnpjNumeros) && HasValidDigits(cnpjNumeros);
        }

        private static bool HasValidSize(string valor)
        {
            return valor.Length == CnpjSize;
        }

        private static bool HasRepeatedDigits(string valor)
        {
            string[] invalidNumbers =
            {
                "00000000000000",
                "11111111111111",
                "22222222222222",
                "33333333333333",
                "44444444444444",
                "55555555555555",
                "66666666666666",
                "77777777777777",
                "88888888888888",
                "99999999999999"
            };

            return invalidNumbers.Contains(valor);
        }

        private static bool HasValidDigits(string valor)
        {
            var number = valor.Substring(0, CnpjSize - 2);

            var checkDigit = new CheckDigit(number)
                .WithMultipliersFromUntil(2, 9)
                .Replacing("0", 10, 11);
            var firstDigit = checkDigit.CalculateDigit();
            checkDigit.AddDigit(firstDigit);
            var secondDigit = checkDigit.CalculateDigit();

            return string.Concat(firstDigit, secondDigit) == valor.Substring(CnpjSize - 2, 2);
        }
    }

    public class CheckDigit
    {
        private string _number;
        private const int Module = 11;
        private readonly List<int> _multipliers = new List<int> { 2, 3, 4, 5, 6, 7, 8, 9 };
        private readonly IDictionary<int, string> _replacements = new Dictionary<int, string>();
        private bool _complementaryModule = true;

        public CheckDigit(string number)
        {
            _number = number;
        }

        public CheckDigit WithMultipliersFromUntil(int firstMultiplier, int lastMultiplier)
        {
            _multipliers.Clear();
            for (var i = firstMultiplier; i <= lastMultiplier; i++)
                _multipliers.Add(i);

            return this;
        }

        public CheckDigit Replacing(string substitute, params int[] digits)
        {
            foreach (var i in digits)
            {
                _replacements[i] = substitute;
            }
            return this;
        }

        public void AddDigit(string digit)
        {
            _number = string.Concat(_number, digit);
        }

        public string CalculateDigit()
        {
            return !(_number.Length > 0) ? "" : GetDigitSum();
        }

        private string GetDigitSum()
        {
            var sum = 0;
            for (int i = _number.Length - 1, m = 0; i >= 0; i--)
            {
                var arithmeticProduct = (int)char.GetNumericValue(_number[i]) * _multipliers[m];
                sum += arithmeticProduct;

                if (++m >= _multipliers.Count) m = 0;
            }

            var mod = (sum % Module);
            var result = _complementaryModule ? Module - mod : mod;

            return _replacements.ContainsKey(result) ? _replacements[result] : result.ToString();
        }
    }
}
