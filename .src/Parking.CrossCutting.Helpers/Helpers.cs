﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Parking.CrossCutting.Helpers
{
    public class Helpers
    {
        public static string OnlyNumbers(string value)
        {
            var onlyNumber = "";

            foreach (var s in value)
            {
                if (char.IsDigit(s))
                {
                    onlyNumber += s;
                }
            }

            return onlyNumber.Trim();
        }
    }
}
